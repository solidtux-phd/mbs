use vergen::{vergen, Config};

fn main() {
    vergen(Config::default()).unwrap();
    println!("cargo:rustc-link-lib=gfortran");
    println!("cargo:rustc-link-lib=blas");
    println!("cargo:rustc-link-lib=lapack");
}
