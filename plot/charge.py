#!/usr/bin/env python3

import collections
import json
import os
import subprocess
from glob import glob
from pathlib import Path
from multiprocessing import Process, Queue
import traceback

import masterdb
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm, ListedColormap, BoundaryNorm

figsize = (19.2, 10.8)
overwrite = False
local = False
EPS = np.finfo(float).eps
max_processes = 12

if local:
    data_folder = "out"
    output = "out_plt"
else:
    data_folder = os.path.expandvars("$HOME/Dokumente/phd_data")
    output = os.path.expandvars("$HOME/Dokumente/phd_plots")
try:
    os.makedirs(output)
except:
    pass

base = ['Ψ↑', 'Ψ↓', 'Ψ↓†', '-Ψ↑†']

tags = set()
field = {}
energies = {}
participation = {}


def plot(folder, output_file, output_file_3d, ham, q):
    try:
        with open(f'{folder}/spectrum/data', 'rb') as f:
            spectrum = masterdb.decode_real_data(f.read())
        with open(f'{folder}/magnetization/data', 'rb') as f:
            magnetization = np.transpose(
                masterdb.decode_real_data(f.read()), [1, 0, 2])
        with open(f'{folder}/state/data', 'rb') as f:
            state = masterdb.decode_complex_data(f.read())

        total = np.copy(magnetization)
        magnetic_field = ham["magnetic_field"]
        if 'data' in magnetic_field:
            n, m, _ = total.shape
            for i in range(n):
                for j in range(m):
                    total[i, j, :] += magnetic_field['data']
            magnetic_field = np.linalg.norm(magnetic_field['data'])
        else:
            total[:, :, 2] += magnetic_field
        mag_norm = np.copy(magnetization)
        tot_norm = np.copy(total)
        indices = np.argsort(np.abs(spectrum))
        majorana1 = np.reshape(
            state[:, indices[0]], (ham['size'][1], ham['size'][0], 4))
        majorana2 = np.reshape(
            state[:, indices[1]], (ham['size'][1], ham['size'][0], 4))
        solutions = np.zeros((ham['size'][1], ham['size'][0]))
        for x in range(ham['size'][0]):
            for y in range(ham['size'][1]):
                for c in range(3):
                    mn = np.linalg.norm(magnetization[y, x, :])
                    if mn > EPS:
                        mag_norm[y, x, c] /= mn
                    tn = np.linalg.norm(total[y, x, :])
                    if tn > EPS:
                        tot_norm[y, x, c] /= tn

                j = np.linalg.norm(total[y, x, :])
                if j >= ham['sc_gap']:
                    if ham['mu'] >= np.sqrt(j**2 - ham['sc_gap']**2):
                        solutions[y, x] = 2
                    else:
                        solutions[y, x] = 1
        charge = np.einsum('ijk,ijk->ij', mag_norm,
                        np.cross(np.gradient(mag_norm, axis=0), np.gradient(mag_norm, axis=1)))/(4*np.pi)
        charge_tot = np.einsum('ijk,ijk->ij', tot_norm,
                            np.cross(np.gradient(tot_norm, axis=0), np.gradient(tot_norm, axis=1)))/(4*np.pi)

        fs = list(figsize)
        # if ham['size'][0] < ham['size'][1]:
        #     size = ham['size'][::-1]
        #     charge = np.transpose(charge)
        #     charge_tot = np.transpose(charge_tot)
        #     solutions = np.transpose(solutions)
        #     majorana1 = np.transpose(majorana1, (1, 0, 2))
        #     majorana2 = np.transpose(majorana2, (1, 0, 2))
        #     magnetization = np.transpose(magnetization, (1, 0, 2))[
        #         :, :, [1, 0, 2]]
        # else:
        size = ham['size']
        fs[1] = (size[1]/size[0]) * (5/3) * figsize[0] * 1.2

        fig, ax = plt.subplots(5, 3, sharex=False, sharey=False, figsize=fs)
        ax[0, 0].set_title('magnetization')
        p = ax[0, 0].quiver(magnetization[:, :, 0],
                            magnetization[:, :, 1], magnetization[:, :, 2])
        fig.colorbar(p, ax=ax[0, 0])

        cm_ref = plt.get_cmap('viridis')
        cm = ListedColormap(colors=[cm_ref(i/3.) for i in range(4)])
        norm = BoundaryNorm([-0.5, 0.5, 1.5, 2.5], cm.N)
        ax[1, 0].set_title('solutions')
        p = ax[1, 0].pcolor(solutions, cmap=cm, norm=norm)
        fig.colorbar(p, ax=ax[1, 0], ticks=[0, 1, 2])

        ax[2, 0].set_title('charge')
        p = ax[2, 0].pcolor(charge)
        fig.colorbar(p, ax=ax[2, 0])

        ax[3, 0].set_title('total charge')
        p = ax[3, 0].pcolor(charge_tot)
        fig.colorbar(p, ax=ax[3, 0])

        ax[0, 1].set_title('state 1 ↑')
        p = ax[0, 1].pcolor(np.abs(majorana1[:, :, 0]+majorana1[:, :, 3]))
        fig.colorbar(p, ax=ax[0, 1])

        ax[0, 2].set_title('state 1 ↑')
        p = ax[0, 2].pcolor(np.abs(majorana1[:, :, 0]-majorana1[:, :, 3]))
        fig.colorbar(p, ax=ax[0, 2])

        ax[1, 1].set_title('state 1 ↓')
        p = ax[1, 1].pcolor(np.abs(majorana1[:, :, 1]+majorana1[:, :, 2]))
        fig.colorbar(p, ax=ax[1, 1])

        ax[1, 2].set_title('state 1 ↓')
        p = ax[1, 2].pcolor(np.abs(majorana1[:, :, 1]-majorana1[:, :, 2]))
        fig.colorbar(p, ax=ax[1, 2])

        ax[2, 1].set_title('state 2 ↑')
        p = ax[2, 1].pcolor(np.abs(majorana2[:, :, 0]+majorana2[:, :, 3]))
        fig.colorbar(p, ax=ax[2, 1])

        ax[2, 2].set_title('state 2 ↑')
        p = ax[2, 2].pcolor(np.abs(majorana2[:, :, 0]-majorana2[:, :, 3]))
        fig.colorbar(p, ax=ax[2, 2])

        ax[3, 1].set_title('state 2 ↓')
        p = ax[3, 1].pcolor(np.abs(majorana2[:, :, 1]+majorana2[:, :, 2]))
        fig.colorbar(p, ax=ax[3, 1])

        ax[3, 2].set_title('state 2 ↓')
        p = ax[3, 2].pcolor(np.abs(majorana2[:, :, 1]-majorana2[:, :, 2]))
        fig.colorbar(p, ax=ax[3, 2])

        for i in range(3):
            ax[4, i].set_title(f'magnetization[{i}]')
            p = ax[4, i].imshow(magnetization[:, :, i], aspect='auto')
            fig.colorbar(p, ax=ax[4, i])
        for i in range(len(ax)):
            for j in range(len(ax[i])):
                ax[i, j].set_aspect('equal', 'box')

        e1 = np.abs(spectrum[indices[0]] - spectrum[indices[1]])/2.
        e2 = np.abs(spectrum[indices[2]] - spectrum[indices[3]])/2.
        field.setdefault(folder_name, []).append(magnetic_field)
        energies.setdefault(folder_name, []).append(spectrum)
        participation.setdefault(folder_name, []).append(
            1/np.sum(np.abs(state)**4, axis=0))

        fig.suptitle(
            f'E2/E1={e2/e1:.2}\nB={magnetic_field:.2} E1={e1:.2e} E2={e2:.2e}')
        plt.tight_layout()
        plt.savefig(output_file)

        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111, projection='3d')
        x, y = np.meshgrid(
            np.arange(magnetization.shape[1]), np.arange(magnetization.shape[0]))
        z = np.zeros_like(x)
        ax.quiver(x, y, z, magnetization[:, :, 0],
                magnetization[:, :, 1], magnetization[:, :, 2])
        ax.set_xlim(0, magnetization.shape[1]-1)
        ax.set_ylim(0, magnetization.shape[0]-1)
        ax.set_zlim(-3, 3)
        ax.set_box_aspect((magnetization.shape[1], magnetization.shape[0], 6))
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        plt.savefig(output_file_3d)
        subprocess.run(["mogrify", "-trim", output_file_3d])
    finally:
        q.put(())


if __name__ == '__main__':
    for base_folder in sorted(glob(f'{data_folder}/*')):
        print(base_folder)
        queues = collections.deque()
        handles = []
        for folder in sorted(glob(f'{base_folder}/*')):
            if '_energy' in folder or 'effective1d' in folder:
                continue
            if not os.path.exists(f'{folder}/state/info.json'):
                continue
            try:
                with open(f'{folder}/state/info.json') as f:
                    config = json.load(f)
                    params = config['parameters']
                    try:
                        ham = params['hamiltonians'][0]
                    except:
                        ham = params['hamiltonian']
                    size_aspect = ham['size'][1]/ham['size'][0]
                tag = params['tag']
                # if not 'new-test' in tag:
                #     continue
                folder_name = folder.split('/')[-2]
                tags.add(folder_name)
                path = f'{output}/{folder_name}'
                output_file = f'{path}/{tag}_{params["index"]:05}.png'
                output_file_3d = f'{path}/3d_{tag}_{params["index"]:05}.png'
                if os.path.exists(output_file) and os.path.exists(output_file_3d) and not overwrite:
                    continue
                q = Queue()
                Path(path).mkdir(parents=True, exist_ok=True)
                h = Process(target=plot, args=(
                    folder, output_file, output_file_3d, ham, q))
                h.start()
                handles.append(h)
                queues.append(q)
                while len(queues) > max_processes:
                    queues.popleft().get()
                for h in handles:
                    try:
                        h.close()
                    except:
                        pass
            except KeyboardInterrupt:
                exit()
            except:
                traceback.print_exc()
        while len(queues) > 0:
            queues.popleft().get()
        for h in handles:
            try:
                h.join()
                h.close()
            except:
                pass
