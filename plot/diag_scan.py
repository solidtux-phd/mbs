#!/usr/bin/env python3

import collections
import json
import os
from glob import glob
from pathlib import Path
from multiprocessing import Process, Queue
import traceback

import masterdb
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.axes_grid1 import make_axes_locatable

# figsize = (19.2, 10.8)
figsize = (30, 30)
overwrite = True
local = False
EPS = np.finfo(float).eps
max_processes = 12
n_max = 8

if local:
    data_folder = "out"
    output = "out_plt"
else:
    data_folder = os.path.expandvars("$HOME/Dokumente/phd_data")
    output = os.path.expandvars("$HOME/Dokumente/phd_plots")
try:
    os.makedirs(output)
except:
    pass

base = ['Ψ↑', 'Ψ↓', 'Ψ↓†', '-Ψ↑†']

tags = set()
field = {}
energies = {}
participation = {}

def colorbar(mappable):
    last_axes = plt.gca()
    ax = mappable.axes
    fig = ax.figure
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = fig.colorbar(mappable, cax=cax)
    cbar.set_ticks([])
    plt.sca(last_axes)
    return cbar

def plot(folder, output_file, ham, q):
    with open(f'{folder}/spectrum/data', 'rb') as f:
        spectrum = masterdb.decode_real_data(f.read())
    with open(f'{folder}/magnetization/data', 'rb') as f:
        magnetization = np.transpose(
            masterdb.decode_real_data(f.read()), [1, 0, 2])
    with open(f'{folder}/state/data', 'rb') as f:
        state = masterdb.decode_complex_data(f.read())

    _, m, _ = magnetization.shape
    magnetic_field = ham["magnetic_field"]
    if 'data' in magnetic_field:
        magnetic_field = np.linalg.norm(magnetic_field['data'])
    indices = np.argsort(np.abs(spectrum))
    n_ind = min(n_max, len(indices))
    for i in range(0, n_ind, 2):
        if spectrum[indices[i + 1]] < spectrum[indices[i]]:
            tmp = indices[i]
            indices[i] = indices[i + 1]
            indices[i + 1] = tmp
    majorana = np.array(list(map(lambda i: np.reshape(
    state[:, i], (ham['size'][1], ham['size'][0], 4)), indices)))
    slice_ind = np.argmax(np.sum(np.abs(majorana), axis=(0, 1, 3)))
    slice = list(map(lambda x: np.abs(x[:, slice_ind]), majorana))

    e1 = np.abs(spectrum[indices[0]] - spectrum[indices[1]])/2.
    e2 = np.abs(spectrum[indices[2]] - spectrum[indices[3]])/2.
    fs = list(figsize)
    size = ham['size']
    rows = 2
    fs[1] = (size[1]/size[0]) * (rows/n_ind) * figsize[0] * 1.2

    fig, ax = plt.subplots(rows, n_ind, sharex=False, sharey=False, figsize=fs)

    for i in range(n_ind):
        ax[0, i].set_title(f'state {i} ↑')
        p = ax[0, i].pcolor(np.abs(majorana[i, :, :, 0]+majorana[i, :, :, 3]))
        colorbar(p)
        ax[0, i].set_aspect('equal')

        ax[1, i].set_title(f'{spectrum[indices[i]]}')
        p = ax[1, i].pcolor(np.abs(majorana[i, :, :, 1]-majorana[i, :, :, 2]))
        colorbar(p)
        ax[1, i].set_aspect('equal')

        # ax[2, i].set_title(f'slice {i} ({slice_ind})')
        # ax[2, i].plot(np.abs(majorana[i, :, slice_ind, 0]+majorana[i, :, slice_ind, 3]))
        # ax[2, i].plot(np.abs(majorana[i, :, slice_ind, 1]-majorana[i, :, slice_ind, 2]))

        # ax[3, i].plot(np.abs(majorana[i, :, slice_ind, 0]))
        # ax[3, i].plot(np.abs(majorana[i, :, slice_ind, 3]))
        # ax[4, i].plot(np.angle(majorana[i, :, slice_ind, 0]))
        # ax[4, i].plot(np.angle(majorana[i, :, slice_ind, 3]))
        # ax[5, i].plot(np.abs(majorana[i, :, slice_ind, 1]))
        # ax[5, i].plot(np.abs(majorana[i, :, slice_ind, 2]))
        # ax[6, i].plot(np.angle(majorana[i, :, slice_ind, 1]))
        # ax[6, i].plot(np.angle(majorana[i, :, slice_ind, 2]))

    fig.suptitle(
        f'E2/E1={e2/e1:.2}\nB={magnetic_field:.3} E1={e1:.2e} E2={e2:.2e}')
    plt.tight_layout()
    plt.savefig(output_file)
    q.put((magnetic_field, slice, e1, e2))


if __name__ == '__main__':
    for base_folder in sorted(glob(f'{data_folder}/*')):
        print(base_folder)
        queues = collections.deque()
        handles = []
        for folder in sorted(glob(f'{base_folder}/*')):
            if '_energy' in folder or 'effective1d' in folder:
                continue
            if not os.path.exists(f'{folder}/state/info.json'):
                continue
            try:
                with open(f'{folder}/state/info.json') as f:
                    config = json.load(f)
                    params = config['parameters']
                    try:
                        ham = params['hamiltonians'][0]
                    except:
                        ham = params['hamiltonian']
                    size_aspect = ham['size'][1]/ham['size'][0]
                tag = params['tag']
                folder_name = folder.split('/')[-2]
                tags.add(folder_name)
                path = f'{output}/{folder_name}'
                output_file = f'{path}/{tag}_{params["index"]:05}.png'
                if os.path.exists(output_file) and not overwrite:
                    continue
                q = Queue()
                Path(path).mkdir(parents=True, exist_ok=True)
                h = Process(target=plot, args=(
                    folder, output_file, ham, q))
                h.start()
                handles.append(h)
                queues.append(q)
                while len(queues) > max_processes:
                    queues.popleft().get()
                for h in handles:
                    try:
                        h.close()
                    except:
                        pass
            except KeyboardInterrupt:
                exit()
            except:
                traceback.print_exc()
        while len(queues) > 0:
            queues.popleft().get()
        for h in handles:
            try:
                h.join()
                h.close()
            except:
                pass
