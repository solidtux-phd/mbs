#!/usr/bin/env python3

import json
import os
import collections
import subprocess
import traceback
from glob import glob
from pathlib import Path
from threading import Thread
from multiprocessing import Process, Queue

import masterdb
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable, axes_size
import numpy as np
from matplotlib.colors import BoundaryNorm, ListedColormap, LogNorm

figsize = (19.2, 10.8)
dpi = 100
overwrite = False
animate = False
local = False
show = False
pol_max = 2
EPS = np.finfo(float).eps
PLOT_EPS = 1e-4
max_processes = 6

if local:
    data_folder = "out"
    output = "out_plt"
else:
    data_folder = os.path.expandvars("$HOME/Dokumente/phd_data")
    output = os.path.expandvars("$HOME/Dokumente/phd_plots")
try:
    os.makedirs(output)
except:
    pass

base = ["Ψ↑", "Ψ↓", "Ψ↓†", "-Ψ↑†"]


def colorbar(mappable):
    last_axes = plt.gca()
    ax = mappable.axes
    fig = ax.figure
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = fig.colorbar(mappable, cax=cax)
    cbar.set_ticks([])
    plt.sca(last_axes)
    return cbar


def plot(folder, output_file, ham, q):
    with open(f"{folder}/spectrum/data", "rb") as f:
        spectrum = masterdb.decode_real_data(f.read())
    with open(f"{folder}/magnetization/data", "rb") as f:
        magnetization = np.transpose(
            masterdb.decode_real_data(f.read()), [1, 0, 2])
    with open(f"{folder}/state/data", "rb") as f:
        state = masterdb.decode_complex_data(f.read())

    total = np.copy(magnetization)
    field = ham["magnetic_field"]
    if hasattr(field, "__len__"):
        n, m, _ = total.shape
        for i in range(n):
            for j in range(m):
                total[i, j, :] += field['data']
        field = np.linalg.norm(field['data'])
    else:
        total[:, :, 2] += field
    mag_norm = np.copy(magnetization)
    tot_norm = np.copy(total)
    indices = np.argsort(spectrum)
    solutions = np.zeros((ham["size"][1], ham["size"][0]))
    for x in range(ham["size"][0]):
        for y in range(ham["size"][1]):
            for c in range(3):
                mn = np.linalg.norm(magnetization[y, x, :])
                if mn > EPS:
                    mag_norm[y, x, c] /= mn
                tn = np.linalg.norm(total[y, x, :])
                if tn > EPS:
                    tot_norm[y, x, c] /= tn

            j = np.linalg.norm(total[y, x, :])
            if j >= ham["sc_gap"]:
                if ham["mu"] >= np.sqrt(j ** 2 - ham["sc_gap"] ** 2):
                    solutions[y, x] = 2
                else:
                    solutions[y, x] = 1

    n_states = len(indices)
    fs = list(figsize)
    # if ham["size"][0] < ham["size"][1]:
    size = ham["size"][::-1]
    # else:
    #     size = ham["size"]
    fs[0] = ((size[0] / size[1]) * (n_states / 8) + 0.1) * figsize[1]
    fig, ax = plt.subplots(7, n_states, sharex=False, sharey=False, figsize=fs)
    title = f'Δ={ham["sc_gap"]:.2}  |  B={field:.2}  |  {size[0]}x{size[1]}  | '
    pol_values = []

    for i in range(n_states):
        ind = indices[i]
        title += f" {spectrum[ind]:.2}"
        s = np.reshape(state[:, ind], (ham["size"][1], ham["size"][0], 4))
        for j in range(4):
            # if s.shape[0] > s.shape[1]:
            im = ax[j, i].imshow(np.transpose(np.abs(s[:, :, j])))
            # else:
            #     im = ax[j, i].imshow(np.abs(s[:, :, j]))
            # cbar = colorbar(im)
            vmin, vmax = im.get_clim()
            ax[j, i].set_title(f"{base[j]} {vmin:.1f}→{vmax:.1f}")
        pol = (
            -2.0
            * (s[:, :, 0] * np.conj(s[:, :, 3]) + s[:, :, 1] * np.conj(s[:, :, 2]))
            * (ham["size"][0] * ham["size"][1])
        )
        # if s.shape[0] > s.shape[1]:
        pol = np.transpose(pol)
        pold = np.abs(pol)
        pol_values.append(np.sum(pold) / (ham["size"][0] * ham["size"][1]))
        polx = np.real(pol)
        poly = np.imag(pol)
        local_pol_max = max(
            np.nanmax(pold), np.nanmax(np.abs(polx)), np.nanmax(np.abs(poly))
        )
        if pol_max is not None:
            local_pol_max = max(pol_max, local_pol_max)
        pold[pold > local_pol_max] = np.nan
        polx[np.abs(polx) > local_pol_max] = np.nan
        poly[np.abs(poly) > local_pol_max] = np.nan
        im = ax[4, i].imshow(pold, vmin=0, vmax=local_pol_max)
        vmin, vmax = im.get_clim()
        ax[4, i].set_title(f"density {vmin:.1f}→{vmax:.1f}")
        # colorbar(im)
        im = ax[5, i].imshow(polx, vmin=-local_pol_max, vmax=local_pol_max)
        vmin, vmax = im.get_clim()
        ax[5, i].set_title(f"polx {vmin:.1f}→{vmax:.1f}")
        # colorbar(im)
        im = ax[6, i].imshow(poly, vmin=-local_pol_max, vmax=local_pol_max)
        vmin, vmax = im.get_clim()
        ax[6, i].set_title(f"poly {vmin:.1f}→{vmax:.1f}")
        # colorbar(im)

    fig.suptitle(title)

    for i in range(len(ax)):
        for j in range(len(ax[i])):
            ax[i, j].get_xaxis().set_visible(False)
            ax[i, j].get_yaxis().set_visible(False)
            ax[i, j].set_aspect("equal", "box")

    plt.tight_layout()
    plt.savefig(output_file, dpi=dpi)
    if show:
        plt.show()
    q.put([field, ham["sc_gap"], *pol_values])


def plot_cummulative(data, output, folder_name, names=["field", "gap"]):
    try:
        num = len(names)
        for (j, name) in enumerate(names):
            d = np.zeros((data.shape[0], data.shape[1] - 1))
            d[:, 0] = data[:, j]
            if np.nanmax(d[:, 0]) - np.nanmin(d[:, 0]) < PLOT_EPS:
                continue
            d[:, 1:] = data[:, num:]
            d.sort(axis=0)
            plt.figure(figsize=figsize)
            for j in range(1, d.shape[1]):
                plt.plot(d[:, 0], d[:, j], "o-", label=f"{j}")
            plt.legend()
            plt.xlabel(name)
            plt.ylabel("total majorana polarization")
            plt.tight_layout()
            plt.savefig(f"{output}/edge_{name}__{folder_name}.png", dpi=dpi)
    except:
        pass


if __name__ == "__main__":
    folders = set()

    for base_folder in sorted(glob(f"{data_folder}/*")):
        if os.path.isfile(base_folder):
            continue
        print(base_folder)
        cummulative_data = []
        folder_name = base_folder.split("/")[-1]
        queues = collections.deque()
        handles = []
        for folder in sorted(glob(f"{base_folder}/*")):
            if '_energy' in folder or 'effective1d' in folder:
                continue
            polarizations = []
            try:
                with open(f"{folder}/state/info.json") as f:
                    config = json.load(f)
                    params = config["parameters"]
                    try:
                        ham = params["hamiltonians"][0]
                    except:
                        ham = params["hamiltonian"]
                    size_aspect = ham["size"][1] / ham["size"][0]
                tag = params["tag"]
                # if not ('edge' in tag or 'charge' in tag):
                # if not 'edge' in tag:
                # if not "paper" in tag:
                #     continue
                path = f"{output}/edge__{folder_name}"
                output_file = f'{path}/{tag}_{params["index"]:05}.png'
                if os.path.exists(output_file) and not overwrite:
                    continue
                print(f"\t{folder}")
                folders.add(path)
                Path(path).mkdir(parents=True, exist_ok=True)
                q = Queue()
                h = Process(target=plot, args=(folder, output_file, ham, q))
                h.start()
                handles.append(h)
                queues.append(q)
                while len(queues) > max_processes:
                    cummulative_data.append(queues.popleft().get())
                for h in handles:
                    try:
                        h.close()
                    except:
                        pass
            except KeyboardInterrupt:
                exit()
            except:
                traceback.print_exc()
        while len(queues) > 0:
            cummulative_data.append(queues.popleft().get())
        for h in handles:
            try:
                h.join()
                h.close()
            except:
                pass
        cummulative_data = np.array(cummulative_data)
        h = Process(
            target=plot_cummulative,
            args=(cummulative_data, output, folder_name),
        )
        h.start()
        h.join()

    if animate:
        for folder in folders:
            print(f"GIF: {folder}")
            num = len(glob(f"{folder}/*.png"))
            args = [
                "convert",
                "-delay",
                f"{1000//num:d}",
                "-loop",
                "0",
                "*.png",
                "animation.gif",
            ]
            print(args)
            subprocess.run(args, cwd=folder)
            subprocess.run(
                ["ffmpeg", "-y", "-i", "animation.gif", "animation.mp4"], cwd=folder
            )
