#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap, BoundaryNorm
from scipy.optimize import minimize
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from mpl_toolkits.axes_grid1 import make_axes_locatable
import warnings
from glob import glob
import os

cut = 0.05
limit = 2
perc = 90
env = 1
eps = 1e-11
plt_nx = 2
plt_ny = 1
names = [['wang', 'box']]
overwrite = False

M = 1.
A = 1.
MU = 0.
DELTA = 1.
ALPHA = 0.3
WIDTH = 1. / A
SCALE = 1. / A
MAGNETIZATION = 1.
T = 1. / (2. * M * A * A)

BMIN = 0.
BMAX = 1.6
MMIN = 0.
MMAX = 1.6

DATA = os.path.expandvars("$HOME/Dokumente/phd_data")
OUTPUT = os.path.expandvars("$HOME/Dokumente/phd_plots")

BC = np.sqrt(MU**2+DELTA**2)

# https://stackoverflow.com/a/68558547/5498356
def interpolate_missing_pixels(
        image: np.ndarray,
):
    from scipy import interpolate

    image = np.ma.masked_invalid(image)
    h, w = image.shape[:2]
    xx, yy = np.meshgrid(np.arange(w), np.arange(h))

    known_x = xx[~image.mask]
    known_y = yy[~image.mask]
    known_v = image[~image.mask]
    missing_x = xx[image.mask]
    missing_y = yy[image.mask]

    try:
        interp_values = interpolate.griddata(
            (known_x, known_y), known_v, (missing_x, missing_y),
            method='cubic'
        )

        interp_image = image.copy()
        interp_image[missing_y, missing_x] = interp_values

        return interp_image
    except Exception as e:
        print(e)
        return image

def f(x, y):
    return np.abs((x-y)**2 - ((MU + y**4 / ((x**2+y**2)**2 * 2 * M * SCALE**2 * np.sinh(WIDTH/SCALE)**2))**2 + DELTA**2))

def colorbar(mappable):
    last_axes = plt.gca()
    ax = mappable.axes
    fig = ax.figure
    # divider = make_axes_locatable(ax)
    # cax = divider.append_axes("bottom", size="5%", pad=-0.1)
    cbar = fig.colorbar(mappable, ax=ax, orientation='horizontal', fraction=0.05, pad=0.1)
    plt.sca(last_axes)
    return cbar

def load(f):
    try:
        data = np.genfromtxt(f, delimiter=',', filling_values=np.nan)
    except ValueError:
        data = np.genfromtxt(f, delimiter=',', filling_values=np.nan, skip_footer=1)
    data = np.atleast_2d(data)
    (nx, ny) = data.shape
    aspect = nx/ny
    nx //= plt_nx
    ny //= plt_ny
    cutx = int(nx * cut)
    cuty = int(ny * cut)
    for i in range(plt_nx):
        for j in range(plt_ny):
            data[nx*j:nx*j+cutx,ny*i:ny*i+cuty] = np.nan
    # tmp = np.copy(data)
    # for i in range(nx-env):
    #     for j in range(ny-env):
    #         e = tmp[i:i+1+2*env,j:j+1+2*env]
    #         e[env,env] = np.nan
    #         with warnings.catch_warnings():
    #             warnings.simplefilter("ignore")
    #             if np.abs(data[i, j]) > limit*(np.nanmedian(np.abs(e)) + 1.):
    #                 data[i,j] = np.nan
    # print(np.nanpercentile(np.abs(data), perc))
    # data[np.abs(data) > limit*np.nanpercentile(np.abs(data), perc)] = np.nan
    return data

def mask(data):
    (ny, nx) = data.shape
    x = np.linspace(BMIN, BMAX, nx)
    y = np.linspace(MMIN, MMAX, ny)
    xv, yv = np.meshgrid(x, y)
    data[yv < xv - BC] = np.nan
    data[yv > xv + BC] = np.nan

b = np.linspace(BMIN, BMAX, 100)
m10 = np.zeros_like(b)
m11 = np.zeros_like(b)
for i in range(len(b)):
    m10[i] = minimize(lambda x: f(-b[i], x), -b[i] + BC).x
    m11[i] = minimize(lambda x: f(-b[i], x), -b[i] - BC).x

def plot(data):
    folder_name = data.split('/')[-1]
    output = f'{OUTPUT}/{folder_name}'
    files = sorted(glob(f'{data}/*.csv'))
    sgn = load(f'{data}/sgn4.csv')
    mask(sgn)

    try:
        os.makedirs(f'{output}')
    except:
        pass
    for f in files:
        print(f)
        name = os.path.basename(f).split('.')[0]
        out_name = f'{output}/{name}.png'
        print('\t', out_name)
        if not overwrite:
            if os.path.exists(out_name):
                if max(os.path.getmtime(f), os.path.getmtime(__file__)) <= os.path.getmtime(out_name):
                    print(f'{name} up to date')
                    continue
        data = load(f)
        (ny, nx) = data.shape
        ny //= plt_ny
        nx //= plt_nx
        aspect = ny/nx
        zero = np.zeros_like(data)
        zero[np.abs(data) < eps] = 1.
        fig, axs = plt.subplots(3*plt_ny, plt_nx, figsize=(10.8*plt_nx, 10.8*3*plt_ny))
        axs = np.atleast_2d(axs)
        for i in range(plt_nx):
            for j in range(plt_ny):
                d = data[ny*j:ny*(j+1),nx*i:nx*(i+1)]
                s = sgn[ny*j:ny*(j+1),nx*i:nx*(i+1)]
                for (ax, d, sign) in [
                    (axs[j, i], d, False),
                    (axs[j + plt_ny, i], np.log10(np.abs(d)), False),
                    (axs[j + 2*plt_ny, i], np.sign(d), True),
                ]:
                    if 'sgn' in name or sign:
                        cmap = 'bwr'
                        scol = 'white'
                        lcol = 'black'
                        mask(d)
                        vmin = -1
                        vmax = 1
                    elif 'tmu' in name:
                        cmap = 'seismic'
                        scol = 'black'
                        lcol = 'red'
                        vmax = max(np.nanmax(d), np.abs(np.nanmin(d)))
                        vmin = -vmax
                    elif 'chern' in name:
                        cmap = 'plasma'
                        # cmap = 'seismic'
                        scol = 'white'
                        lcol = 'red'
                        # d = np.abs(d)
                        # d = np.mod(d, 2)
                        # mask(d)
                        # vmax = np.nanmax(d)
                        # vmax = 1.
                        # vmin = 0.
                        # vmax = max(np.nanmax(d), np.abs(np.nanmin(d)))
                        # vmin = -vmax
                        vmin = min(0., np.nanmin(d))
                        vmax = max(1., np.nanmax(d))
                    else:
                        cmap = 'viridis'
                        scol = 'white'
                        lcol = 'red'
                        vmin = np.nanmin(d)
                        vmax = np.nanmax(d)
                    d[d > vmax] = np.nan
                    d[d < vmin] = np.nan
                    im = ax.imshow(interpolate_missing_pixels(d), interpolation='none', extent=(BMIN, BMAX, MMIN, MMAX), origin='lower', aspect=aspect, vmin=vmin, vmax=vmax, cmap=cmap)
                    ax.autoscale(False, axis='both')
                    if not 'sgn' in name:
                        # ax.contour(d, levels=[0, 0.01], extent=(BMIN, BMAX, MMIN, MMAX), origin='lower', colors=['magenta', 'orange'])
                        ax.contour(s, levels=[0], extent=(BMIN, BMAX, MMIN, MMAX), origin='lower', colors=scol)
                    ax.plot(b, b + BC, c=lcol, lw=4, scaley=False)
                    ax.plot(b, b - BC, c=lcol, lw=4, scaley=False)
                    ax.plot(b, m10, c=lcol, ls='--', lw=4, scaley=False)
                    ax.plot(b, m11, c=lcol, ls='--', lw=4, scaley=False)
                    ax.axvline(x=BC, c=lcol, ls=':', lw=4)
                    colorbar(im)
                    ax.set_xlabel('-B')
                    ax.set_ylabel('J|M|')
                    ax.set_title(names[j][i])
        fig.suptitle(name)
        plt.tight_layout()
        plt.savefig(out_name, dpi=100)
        plt.close('all')

for folder in glob(f'{DATA}/*effective1d*'):
    if os.path.isdir(folder):
        try:
            plot(folder)
        except Exception as e:
            print(e)