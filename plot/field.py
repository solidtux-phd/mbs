#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

figsize = (19.2, 10.8)
folder = '/home/daniel/Dokumente/phd_data'
output = '/home/daniel/Dokumente/phd_plots'

for name in ['field', 'field_large']:
    try:
        plt.figure(figsize=figsize)
        data = np.atleast_2d(np.genfromtxt(
            f'{folder}/{name}.csv', delimiter=','))
        plt.plot(data[:, 0], data[:, 1], 'o-')
        plt.plot(data[:, 0], data[:, 2], 'o-')
        plt.plot(data[:, 0], -data[:, 1], 'o-')
        plt.plot(data[:, 0], -data[:, 2], 'o-')
        plt.tight_layout()
        plt.savefig(f'{output}/{name}.png')
        plt.close()
    except Exception as e:
        print(e)
