#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

data = np.atleast_2d(np.genfromtxt('length.csv', delimiter=','))
plt.semilogy(data[:,0], data[:,1], 'o-')
plt.semilogy(data[:,0], data[:,2], 'o-')
plt.tight_layout()
plt.savefig('length.png')
plt.show()
