#!/bin/bash

set -ex

./plot/paper_domainwall.py
cp ~/Dokumente/phd_plots/paper-domainwall__2021-06-21_13-29-17_paper-domainwall3-medium-001/* ~/Dokumente/phd/papers/domainwall/data
pushd ~/Dokumente/phd/paper/domainwall
make main.pdf