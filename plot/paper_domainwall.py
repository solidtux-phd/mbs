#!/usr/bin/env python3

import json
import os
import traceback
from glob import glob
from pathlib import Path
import sys

import masterdb
import numpy as np
from scipy.interpolate import griddata

overwrite = True
local = False
EPS = np.finfo(float).eps
if len(sys.argv) > 1:
    only = sys.argv[1]
else:
    only = None
plot = False
border_res = (400, 100)
margin = 10

if local:
    data_folder = "out"
    output = "out_plt"
else:
    data_folder = os.path.expandvars("$HOME/Dokumente/phd_data")
    output = os.path.expandvars("$HOME/Dokumente/phd_plots")
try:
    os.makedirs(output)
except:
    pass

base = ["Ψ↑", "Ψ↓", "Ψ↓†", "-Ψ↑†"]

ampnames = ["\\maxampsingle", "\\maxampdouble"]
ampkeys = ["max_amp_1", "max_amp_2"]
dennames = ["\\maxdensingle", "\\maxdendouble"]
denkeys = ["max_den_1", "max_den_2"]

counter = {}
counter_en = {}
texconfig = {}

for base_folder in reversed(sorted(glob(f"{data_folder}/*"))):
    if os.path.isfile(base_folder):
        continue
    if only is not None:
        if not only in base_folder:
            continue
    elif not "paper-domainwall" in base_folder:
        # if not "new-test" in base_folder:
        continue
    print(base_folder)
    energy = "_energy" in base_folder
    folder_name = base_folder.split("/")[-1].removesuffix("_energy")
    path = f"{output}/paper-domainwall__{folder_name}"
    local_config = texconfig.setdefault(path, {})
    for key in ampkeys:
        local_config.setdefault(key, 0)
    for key in denkeys:
        local_config.setdefault(key, 0)
    Path(path).mkdir(parents=True, exist_ok=True)
    if energy:
        ftex = open(f"{path}/data_en.tex", "w")
    else:
        fen = open(f"{path}/energies.txt", "w")
        fpol = open(f"{path}/polarizations.txt", "w")
        ftex = open(f"{path}/data.tex", "w")
    for folder in sorted(glob(f"{base_folder}/*")):
        try:
            with open(f"{folder}/spectrum/info.json") as f:
                config = json.load(f)
                params = config["parameters"]
                try:
                    ham = params["hamiltonians"][0]
                except:
                    ham = params["hamiltonian"]
            size_aspect = ham["size"][1] / ham["size"][0]
            tag = params["tag"]
            maglist = ham["magnetization"]
            if "combine" in maglist:
                maglist = list(
                    map(lambda x: x[0], maglist["combine"]["magnetizations"])
                )
            elif "constant" in maglist:
                if "magnetizations" in maglist["constant"]:
                    maglist = list(
                        map(lambda x: x[0], maglist["constant"]["magnetizations"])
                    )
                else:
                    maglist = [maglist]
            else:
                maglist = [maglist]
            try:
                rotation = ""
                for m in maglist:
                    rotation += f'_{int(m["domain_wall"]["rotation"]*180/np.pi)}'
                rotation = rotation[1:]
            except KeyError:
                rotation = "none"
            try:
                if ham["magnetization_modifier"] is not None:
                    boundary = "es"
                elif ham["sc_gap_modifier"] is not None:
                    boundary = "em"
                else:
                    boundary = "same"
            except KeyError:
                boundary = "none"
            try:
                structure = ""
                for m in maglist:
                    s = list(m['domain_wall']['structure'].keys())[0]
                    structure += f'_{s}'
                structure = structure[1:]
            except KeyError:
                structure = "none"
            try:
                if ham["magnetization_modifier"] is not None:
                    inner_height = 2 * int(
                        ham["magnetization_modifier"]["Rectangle"]["height"]
                    )
                elif ham["sc_gap_modifier"] is not None:
                    inner_height = 2 * int(
                        ham["sc_gap_modifier"]["Rectangle"]["height"]
                    )
                else:
                    inner_height = ham["size"][1]
            except:
                inner_height = ham["size"][1]
            print(f"\t{folder}")
            with open(f"{folder}/spectrum/data", "rb") as f:
                spectrum = masterdb.decode_real_data(f.read())
            with open(f"{folder}/magnetization/data", "rb") as f:
                magnetization = np.transpose(
                    masterdb.decode_real_data(f.read()), [1, 0, 2]
                )
            if os.path.exists(f"{folder}/state/data"):
                with open(f"{folder}/state/data", "rb") as f:
                    state = masterdb.decode_complex_data(f.read())
                indices = list(filter(lambda x: spectrum[x] > 0, np.argsort(spectrum)))[
                    : len(maglist)
                ]
                btot = np.copy(magnetization)
                for i in range(btot.shape[0]):
                    for j in range(btot.shape[1]):
                        if 'data' in ham['magnetic_field']:
                            btot[i, j, :] += ham["magnetic_field"]['data']
                        else:
                            btot[i, j, 2] += ham["magnetic_field"]

                ftex.write("\\begin{figure}\n")
                pold = np.zeros((ham["size"][1], ham["size"][0]))
                amp = np.zeros((ham["size"][1], ham["size"][0]))
                fen.write(f'{params["index"]:05}')
                for ind in indices:
                    fen.write(f" {spectrum[ind]}\n")
                    s = np.reshape(state[:, ind], (ham["size"][1], ham["size"][0], 4))
                    pol = np.abs(
                        -2.0
                        * (
                            s[:, :, 0] * np.conj(s[:, :, 3])
                            - s[:, :, 1] * np.conj(s[:, :, 2])
                        )
                        * (ham["size"][0] * ham["size"][1])
                    )
                    amp += np.sqrt(np.sum(np.abs(s) ** 2, axis=2))
                    pold += pol / (pol.shape[0] * pol.shape[1])
                fpol.write(f'{params["index"]:05} {np.sum(pold)}\n')

                if 'data' in ham['magnetic_field']:
                    n, m, _ = magnetization.shape
                    phi = np.zeros((n, m))
                    for i in range(n):
                        for j in range(m):
                            tmp = magnetization[i, j, :] + ham['magnetic_field']['data']
                            phi[i, j] = np.arctan2(tmp[0], tmp[1])
                else:
                    phi = np.arctan2(
                        magnetization[:, :, 1],
                        magnetization[:, :, 2] + ham["magnetic_field"],
                    )
                dphi = np.gradient(phi)
                mu_eff = ham["mu"] + 0.25 * ham["t"] * (dphi[0] ** 2 + dphi[1] ** 2)
                border_scale = max(
                    border_res[0] / phi.shape[0], border_res[1] / phi.shape[0]
                )
                topological = np.zeros(
                    (phi.shape[0] + 2 * margin, phi.shape[1] + 2 * margin)
                )
                topological[
                    margin : margin + phi.shape[0], margin : margin + phi.shape[1]
                ] = np.heaviside(
                    np.sqrt(np.sum(np.abs(btot) ** 2, axis=2))
                    - np.sqrt(mu_eff ** 2 + ham["sc_gap"] ** 2),
                    1,
                )
                for mind in range(margin):
                    topological[margin : margin + phi.shape[0], mind] = topological[
                        margin : margin + phi.shape[0], margin
                    ]
                    topological[mind, margin : margin + phi.shape[1]] = topological[
                        margin, margin : margin + phi.shape[1]
                    ]
                    topological[
                        margin : margin + phi.shape[0], margin + phi.shape[1] + mind
                    ] = topological[
                        margin : margin + phi.shape[0], margin + phi.shape[1] - 1
                    ]
                    topological[
                        margin + phi.shape[0] + mind, margin : margin + phi.shape[1]
                    ] = topological[
                        margin + phi.shape[0] - 1, margin : margin + phi.shape[1]
                    ]
                topological[0:margin, 0:margin] = topological[margin, margin]
                topological[margin + phi.shape[0] :, 0:margin] = topological[
                    margin + phi.shape[0] - 1, margin
                ]
                topological[0:margin, margin + phi.shape[1] :] = topological[
                    margin, margin + phi.shape[1] - 1
                ]
                topological[
                    margin + phi.shape[0] :, margin + phi.shape[1] :
                ] = topological[margin + phi.shape[0] - 1, margin + phi.shape[1] - 1]

                x0, y0 = np.meshgrid(
                    np.arange(topological.shape[1]), np.arange(topological.shape[0])
                )
                interp_x, interp_y = np.meshgrid(
                    np.linspace(
                        0,
                        topological.shape[1] - 1,
                        int(border_scale * topological.shape[1]),
                    ),
                    np.linspace(
                        0,
                        topological.shape[0] - 1,
                        int(border_scale * topological.shape[0]),
                    ),
                )
                topological = griddata(
                    (x0.flatten(), y0.flatten()),
                    topological.flatten(),
                    (interp_x, interp_y),
                    method="nearest",
                )

                top_grad = np.gradient(topological)
                border = np.heaviside(top_grad[0] ** 2 + top_grad[1] ** 2, 0)

                key = (rotation, boundary, structure)
                if key in counter:
                    counter[key] += 1
                else:
                    counter[key] = 0
                index = counter[key]
                print(folder, index, key)
                filename = f"{rotation}_{structure}_{boundary}_{index}"
                ampname = ampnames[len(maglist) - 1]
                ampkey = ampkeys[len(maglist) - 1]
                denname = dennames[len(maglist) - 1]
                denkey = denkeys[len(maglist) - 1]
                ftex.write(
                    f"\t\\plot{{{filename}}}{{{amp.shape[0]}}}{{{amp.shape[1]}}}{{{inner_height}}}{{{ampname}}}{{{denname}}}\n"
                )
                print(f"\t\t{filename}")
                local_config[ampkey] = max(local_config[ampkey], np.nanmax(amp))
                local_config[denkey] = max(local_config[denkey], np.nanmax(pold))
                with open(f"{path}/{filename}.txt", "w") as f:
                    for y in range(amp.shape[0]):
                        for x in range(amp.shape[1]):
                            for (dx, dy) in [
                                (-0.5, -0.5),
                                (0.5, -0.5),
                                (0.5, 0.5),
                                (-0.5, 0.5),
                            ]:
                                f.write(
                                    f"{x+dx} {y+dy} {amp[y,x]} {pold[y,x]} {topological[y,x]} {border[y,x]}\n"
                                )
                dx0 = interp_x[0, 1] - interp_x[0, 0]
                dy0 = interp_y[1, 0] - interp_y[0, 0]
                with open(f"{path}/{filename}_topo.txt", "w") as f:
                    for i in range(border.shape[0]):
                        for j in range(border.shape[1]):
                            for (dx, dy) in [
                                (-0.5, -0.5),
                                (0.5, -0.5),
                                (0.5, 0.5),
                                (-0.5, 0.5),
                            ]:
                                if border[i, j] > 0:
                                    x = interp_x[i, j] + dx * dx0 - margin
                                    y = interp_y[i, j] + dy * dy0 - margin
                                    f.write(f"{x} {y}\n")
                ftex.write("\\end{figure}\n")
                with open(f"{path}/{filename}_cutx.txt", "w") as f:
                    y = np.argmax(np.sum(amp, axis=1))
                    for x in range(amp.shape[1]):
                        f.write(f"{x} {amp[y,x]}\n")
                    ftex.write(
                        f"\\begin{{figure}}\n\t\\plotcut{{{filename}_cutx}}{{x}}\n\\end{{figure}}\n"
                    )
                with open(f"{path}/{filename}_cuty.txt", "w") as f:
                    x = np.argmax(np.sum(amp, axis=0))
                    for y in range(amp.shape[0]):
                        f.write(f"{y} {amp[y,x]}\n")
                    ftex.write(
                        f"\\begin{{figure}}\n\t\\plotcut{{{filename}_cuty}}{{y}}\n\\end{{figure}}\n"
                    )
                ftex.write("\\clearpage\n")
            else:
                key = (rotation, structure, boundary)
                if key in counter_en:
                    counter_en[key] += 1
                else:
                    counter_en[key] = 0
                index = counter_en[key]
                filename = f"{rotation}_{structure}_{boundary}_{index}_energy"
                print(f"\t\t{filename}")
                np.savetxt(f"{path}/{filename}.txt", spectrum)
                ftex.write(
                    f"\\begin{{figure}}\n\t\\ploten{{{filename}}}\n\\end{{figure}}\n"
                )
                ftex.write("\\clearpage\n")
        except KeyboardInterrupt:
            exit(1)
        except:
            traceback.print_exc()

for (path, config) in texconfig.items():
    with open(f"{path}/config.tex", "w") as f:
        for i in range(len(ampnames)):
            f.write(f"\\pgfmathsetmacro{{{ampnames[i]}}}{{{config[ampkeys[i]]}}}\n")
        for i in range(len(dennames)):
            f.write(f"\\pgfmathsetmacro{{{dennames[i]}}}{{{config[denkeys[i]]}}}\n")