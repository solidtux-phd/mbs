#!/bin/bash

# FOLDER=2022-03-24_08-59-56_paper-domainwall-001
FOLDER=2022-03-25_09-19-57_paper-domainwall

set -ex

pipenv run ./plot/paper_domainwall.py $FOLDER
rm ~/Dokumente/phd/papers/domainwall/data/* || true
rm ~/Dokumente/phd/papers/domainwall/plots/* || true
cp ~/Dokumente/phd_plots/paper-domainwall__$FOLDER/* ~/Dokumente/phd/papers/domainwall/data
pushd ~/Dokumente/phd/papers/domainwall
make
# make plots.pdf
