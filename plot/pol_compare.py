#!/usr/bin/env python3

import json
import collections
import os
import traceback
from pathlib import Path
from multiprocessing import Process, Queue
from tqdm import tqdm

import masterdb
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np

figsize = (19.2, 10.8)
dpi = 100
overwrite = True
local = False
show = False
pol_max = 2
EPS = np.finfo(float).eps
PLOT_EPS = 1e-4
max_processes = 10

if local:
    data_folder = "out"
    output = "out_plt"
else:
    data_folder = "/home/daniel/Dokumente/phd_data"
    output = "/home/daniel/Dokumente/phd_plots"

base = ["Ψ↑", "Ψ↓", "Ψ↓†", "-Ψ↑†"]

num_states = 3
num_points = [1000, 200, 200]
x_parameter = "magnetic_field"
# skip = [0, 1]
skip = []
folder_lists = [
    [
        ("2021-05-06_16-10-05_strip-border-more-001", "both"),
        ("2021-05-06_16-23-43_strip-border-more-002", "magnet strip"),
        ("2021-05-06_16-48-51_strip-border-more-003", "sc strip"),
    ],
    [
        ("2021-04-29_10-39-43_strip-border-003", "same"),
        ("2021-05-02_22-01-05_strip-border-004", "magnet strip"),
        ("2021-05-03_13-17-56_strip-border-005", "sc strip"),
    ],
    [
        ("2021-05-10_12-14-28_strip-border-006", "same"),
        ("2021-05-10_12-16-30_strip-border-007", "magnet strip"),
        ("2021-05-10_12-17-42_strip-border-008", "sc strip"),
    ],
]


def plot(point_ind: int, q, path, folders):
    values = {}
    output_file = f"{path}/{point_ind:05}.png"
    if os.path.exists(output_file) and not overwrite:
        q.put(values)
        return
    (fig, ax) = plt.subplots(7, num_states * len(folders), figsize=figsize)
    something_plotted = False
    for (folder_ind, (folder_name, folder_title)) in enumerate(folders):
        folder = f"{data_folder}/{folder_name}/{point_ind:05}"
        if not os.path.exists(folder):
            continue
        try:
            with open(f"{folder}/state/info.json") as f:
                config = json.load(f)
                params = config["parameters"]
                try:
                    ham = params["hamiltonians"][0]
                except:
                    ham = params["hamiltonian"]
                size_aspect = ham["size"][1] / ham["size"][0]
            with open(f"{folder}/spectrum/data", "rb") as f:
                spectrum = masterdb.decode_real_data(f.read())
            with open(f"{folder}/magnetization/data", "rb") as f:
                magnetization = np.transpose(
                    masterdb.decode_real_data(f.read()), [1, 0, 2]
                )
            with open(f"{folder}/state/data", "rb") as f:
                state = masterdb.decode_complex_data(f.read())
            if folder_ind == 0:
                if ham["size"][0] < ham["size"][1]:
                    size = ham["size"][::-1]
                else:
                    size = ham["size"]
                fig.suptitle(
                    f'Δ={ham["sc_gap"]:.2}  |  B={ham["magnetic_field"]:.2}  |  {size[0]}x{size[1]}  | '
                )
            indices = list(filter(lambda i: spectrum[i] > 0, np.argsort(spectrum)))
            tmp = [ham[x_parameter]]
            tmp_max = [ham[x_parameter]]
            # tmp = [point_ind]
            # tmp_max = [point_ind]
            for (state_ind, ind) in enumerate(indices[:num_states]):
                ax_ind = len(folders) * state_ind + folder_ind
                s = np.reshape(state[:, ind], (ham["size"][1], ham["size"][0], 4))
                if s.shape[0] > s.shape[1]:
                    s = s.transpose((1, 0, 2))
                for component in range(4):
                    im = ax[component, ax_ind].imshow(np.abs(s[:, :, component]))
                    vmin, vmax = im.get_clim()
                    title = f"{base[component]} {vmin:.1f}→{vmax:.1f}"
                    if component == 0:
                        title = f"{folder_title}\n" + title
                    ax[component, ax_ind].set_title(title)
                pol = (
                    -2.0
                    * (
                        s[:, :, 0] * np.conj(s[:, :, 3])
                        + s[:, :, 1] * np.conj(s[:, :, 2])
                    )
                    * (ham["size"][0] * ham["size"][1])
                )
                if s.shape[0] > s.shape[1]:
                    pol = np.transpose(pol)
                pold = np.abs(pol)
                tmp.append(np.sum(pold) / (ham["size"][0] * ham["size"][1]))
                tmp_max.append(np.nanmax(pold))
                polx = np.real(pol)
                poly = np.imag(pol)
                im = ax[4, ax_ind].imshow(pold)
                vmin, vmax = im.get_clim()
                ax[4, ax_ind].set_title(
                    f"density {vmin:.1f}→{vmax:.1f}\n{np.nanmin(pold):.1f}→{np.nanmax(pold):.1f}"
                )
                im = ax[5, ax_ind].imshow(polx)
                vmin, vmax = im.get_clim()
                ax[5, ax_ind].set_title(f"polx {vmin:.1f}→{vmax:.1f}")
                im = ax[6, ax_ind].imshow(poly)
                vmin, vmax = im.get_clim()
                ax[6, ax_ind].set_title(f"poly {vmin:.1f}→{vmax:.1f}")
            values[folder_title] = (tmp, tmp_max)
        except KeyboardInterrupt:
            q.put(values)
            exit()
        except:
            traceback.print_exc()
        something_plotted = True
    if something_plotted:
        for i in range(len(ax)):
            for j in range(len(ax[i])):
                ax[i, j].get_xaxis().set_visible(False)
                ax[i, j].get_yaxis().set_visible(False)
                ax[i, j].set_aspect("equal", "box")
        plt.tight_layout()
        plt.savefig(output_file, dpi=dpi)
        plt.close(fig)
    q.put(values)


def plot_cummulative(values, ind, i, name):
    print(ind, i, name)
    plt.figure(figsize=figsize)
    for (folder_title, data) in values.items():
        print(folder_title)
        data = np.array(sorted(data))
        data = data[data[:, 0].argsort()]
        plt.plot(data[:, 0], data[:, i + 1], "-o", label=f"{folder_title}")
    plt.legend()
    plt.tight_layout()
    plt.savefig(f"{output}/pol_compare_{ind:05}_{name}_{i:02}.png")
    plt.close()


def colorbar(mappable):
    last_axes = plt.gca()
    ax = mappable.axes
    fig = ax.figure
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = fig.colorbar(mappable, cax=cax)
    cbar.set_ticks([])
    plt.sca(last_axes)
    return cbar


for folder_ind, folders in enumerate(folder_lists):
    if folder_ind in skip:
        continue
    pol_sum_values = {}
    pol_max_values = {}
    handles = []
    queues = collections.deque()
    path = f"{output}/pol_compare_{folder_ind:05}"
    Path(path).mkdir(parents=True, exist_ok=True)
    print(path)
    for point_ind in tqdm(range(num_points[folder_ind])):
        q = Queue()
        h = Process(target=plot, args=(point_ind, q, path, folders))
        h.start()
        handles.append(h)
        queues.append(q)
        while len(queues) > max_processes:
            values = queues.popleft().get()
            for (n, v) in values.items():
                pol_sum_values.setdefault(n, []).append(v[0])
                pol_max_values.setdefault(n, []).append(v[1])
        for h in handles:
            try:
                h.close()
            except:
                pass
    while len(queues) > 0:
        values = queues.popleft().get()
        for (n, v) in values.items():
            pol_sum_values.setdefault(n, []).append(v[0])
            pol_max_values.setdefault(n, []).append(v[1])
    for h in handles:
        try:
            h.join()
            h.close()
        except:
            pass
    for i in range(num_states):
        for (name, values) in [
            ("sum", pol_sum_values),
            ("max", pol_max_values),
        ]:
            h = Process(target=plot_cummulative, args=(values, folder_ind, i, name))
            h.start()
            h.join()