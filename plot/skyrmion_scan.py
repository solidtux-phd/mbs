#!/usr/bin/env python3

import json
import os
from glob import glob
from pathlib import Path
import traceback

import masterdb
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm, Normalize

figsize = (19.2, 10.8)
local = False
show = False
points = True

if local:
    data_folder = 'out'
    output = 'out_plt'
else:
    data_folder = '/home/daniel/Dokumente/phd_data'
    output = '/home/daniel/Dokumente/phd_plots'

base = ['Ψ↑', 'Ψ↓', 'Ψ↓†', '-Ψ↑†']

tags = set()
field = {}
spin_orbit = {}
lower = {}
upper = {}

for folder in sorted(glob(f'{data_folder}/*/*')):
    if not os.path.exists(f'{folder}/spectrum/info.json'):
        continue
    print(folder)
    try:
        with open(f'{folder}/spectrum/info.json') as f:
            config = json.load(f)
            params = config['parameters']
            try:
                ham = params['hamiltonians'][0]
            except:
                ham = params['hamiltonian']
            size_aspect = ham['size'][1]/ham['size'][0]
        tag = params['tag']
        if not 'scan' in tag:
            continue
        tags.add(tag)
        path = f'{output}/{tag}'

        with open(f'{folder}/spectrum/data', 'rb') as f:
            spectrum = masterdb.decode_real_data(f.read())
        with open(f'{folder}/magnetization/data', 'rb') as f:
            magnetization = np.transpose(
                masterdb.decode_real_data(f.read()), [1, 0, 2])

        total = np.copy(magnetization)
        total[:, :, 2] += ham['magnetic_field']
        mag_norm = np.copy(magnetization)
        tot_norm = np.copy(total)
        indices = np.argsort(np.abs(spectrum))

        e1 = (np.abs(spectrum[indices[0]]) + np.abs(spectrum[indices[1]]))/2.
        e2 = np.abs(spectrum[indices[2]])

        field.setdefault(tag, []).append(ham['magnetic_field'])
        spin_orbit.setdefault(tag, []).append(ham['spin_orbit'])
        lower.setdefault(tag, []).append(e1)
        upper.setdefault(tag, []).append(e2)
    except KeyboardInterrupt:
        exit()
    except:
        traceback.print_exc()

for tag in tags:
    if tag in field and tag in lower and tag in upper and tag in spin_orbit:
        fig, ax = plt.subplots(2, 3, figsize=figsize, sharex=True, sharey=True)
        u = np.array(upper[tag])
        l = np.array(lower[tag])

        for (i, (n, l)) in enumerate([('E₂', u), ('E₁', l), ('E₂-E₁', u-l)]):
            for (j, norm) in enumerate([Normalize(), LogNorm()]):
                p = ax[j, i].tricontourf(
                    field[tag], spin_orbit[tag], l, levels=200, cmap=plt.get_cmap('inferno'), norm=norm)
                fig.colorbar(p, ax=ax[j, i])
                if points:
                    ax[j, i].plot(field[tag], spin_orbit[tag], 'ro', ms=0.5)
                ax[j, i].set_xlabel('B')
                ax[j, i].set_ylabel('λ')
                ax[j, i].set_title(n)

        plt.tight_layout()
        plt.savefig(f'{output}/skyrmion_scan_{tag}.png')
        if show:
            plt.show()
        else:
            plt.close()
