#!/usr/bin/env python3

import json
import os
import collections
import subprocess
import traceback
from glob import glob
from pathlib import Path
from threading import Thread
from multiprocessing import Process, Queue

import masterdb
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable, axes_size
import numpy as np
from matplotlib.colors import BoundaryNorm, ListedColormap, LogNorm

figsize = (19.2, 10.8)
dpi = 100
overwrite = False
animate = False
local = False
show = False
pol_max = 2
EPS = np.finfo(float).eps
PLOT_EPS = 1e-4
max_processes = 6

if local:
    data_folder = "out"
    output = "out_plt"
else:
    data_folder = os.path.expandvars("$HOME/Dokumente/phd_data")
    output = os.path.expandvars("$HOME/Dokumente/phd_plots")
try:
    os.makedirs(output)
except:
    pass

base = ["Ψ↑", "Ψ↓", "Ψ↓†", "-Ψ↑†"]

def plot(folder, output_file, ham, q):
    with open(f"{folder}/spectrum/data", "rb") as f:
        spectrum = masterdb.decode_real_data(f.read())
    # with open(f"{folder}/magnetization/data", "rb") as f:
        # magnetization = np.transpose(
        #     masterdb.decode_real_data(f.read()), [1, 0, 2])
    plt.figure(figsize=figsize)
    plt.plot(np.sort(spectrum), 'o')
    plt.xlabel('n')
    plt.ylabel('E')
    plt.tight_layout()
    plt.savefig(output_file, dpi=dpi)
    q.put(())

if __name__ == "__main__":
    folders = set()

    for base_folder in sorted(glob(f"{data_folder}/*_energy")):
        if os.path.isfile(base_folder):
            continue
        print(base_folder)
        folder_name = base_folder.split("/")[-1]
        queues = collections.deque()
        handles = []
        for folder in sorted(glob(f"{base_folder}/*")):
            polarizations = []
            try:
                with open(f"{folder}/spectrum/info.json") as f:
                    config = json.load(f)
                    params = config["parameters"]
                    try:
                        ham = params["hamiltonians"][0]
                    except:
                        ham = params["hamiltonian"]
                    size_aspect = ham["size"][1] / ham["size"][0]
                tag = params["tag"]
                path = f"{output}/spectrum_{folder_name}"
                output_file = f'{path}/{tag}_{params["index"]:05}.png'
                if os.path.exists(output_file) and not overwrite:
                    continue
                print(f"\t{folder}")
                folders.add(path)
                Path(path).mkdir(parents=True, exist_ok=True)
                q = Queue()
                h = Process(target=plot, args=(folder, output_file, ham, q))
                h.start()
                handles.append(h)
                queues.append(q)
                while len(queues) > max_processes:
                    queues.popleft().get()
                for h in handles:
                    try:
                        h.close()
                    except:
                        pass
            except KeyboardInterrupt:
                exit()
            except:
                traceback.print_exc()
        while len(queues) > 0:
            queues.popleft().get()
        for h in handles:
            try:
                h.join()
                h.close()
            except:
                pass