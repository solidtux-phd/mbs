#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

data = np.genfromtxt('test.csv')
plt.imshow(data, aspect='auto')
plt.colorbar()
plt.tight_layout()
plt.show()
