#!/usr/bin/env python3

import json
import os
from glob import glob
from pathlib import Path
import traceback

import masterdb
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm

figsize = (19.2, 10.8)
overwrite = True
local = False
show = False

if local:
    data_folder = 'out'
    output = 'out_plt'
else:
    data_folder = '/home/daniel/Dokumente/phd_data'
    output = '/home/daniel/Dokumente/phd_plots'

base = ['Ψ↑', 'Ψ↓', 'Ψ↓†', '-Ψ↑†']

for folder in glob(f'{data_folder}/*'):
    tag = os.path.basename(folder)
    path = f'{output}/{tag}'
    try:
        Path(path).mkdir(parents=True, exist_ok=True)

        if not overwrite and os.path.exists(f'{path}/spectrum.png'):
            continue

        with open(f'{folder}/spectrum/data', 'rb') as f:
            plt.figure(figsize=figsize)
            spectrum = masterdb.decode_real_data(f.read())
            plt.plot(sorted(spectrum*1e3), 'o-')
            plt.ylabel('E / meV')
            plt.tight_layout()
            plt.savefig(f'{path}/spectrum.png')
            if show:
                plt.show()
            plt.close()
        with open(f'{folder}/magnetization/data', 'rb') as f:
            plt.figure(figsize=figsize)
            magnetization = np.transpose(
                masterdb.decode_real_data(f.read()), [1, 0, 2])
            plt.quiver(
                magnetization[:, :, 0], magnetization[:, :, 1], magnetization[:, :, 2])
            plt.colorbar()
            plt.tight_layout()
            if show:
                plt.show()
            plt.savefig(f'{path}/magnetization.png')
            plt.close()
        with open(f'{folder}/state/info.json') as f:
            config = json.load(f)
            params = config['parameters']
            ham = params['hamiltonians'][0]
            # dx = ham['size'][0]/ham['nx']
            # dy = ham['size'][1]/ham['ny']
            dx = 1
            dy = 1
            aspect = dy/dx
            size_aspect = ham['size'][1]/ham['size'][0]
        with open(f'{folder}/state/data', 'rb') as f:
            state = masterdb.decode_complex_data(f.read())
        for i in range(0, min(len(spectrum), 100)-1, 2):
            print(f'{i:>6} {spectrum[i]} {spectrum[i+1]}')
            fig, ax = plt.subplots(8, 3, figsize=(
                figsize[1]/((8./3.)*size_aspect), figsize[1]))
            fig.suptitle(f'{spectrum[i]} {spectrum[i+1]}')
            s1 = np.reshape(state[:, i], (4, ham['size'][1], ham['size'][0]))
            s2 = np.reshape(state[:, i+1], (4, ham['size'][1], ham['size'][0]))

            for j in range(4):
                ax[j, 0].set_title(base[j])
                p = ax[j, 0].imshow(np.abs(s1[j, :, :]), aspect=aspect)
                fig.colorbar(p, ax=ax[j, 0])
                ax[j+4, 0].set_title(base[j])
                p = ax[j+4, 0].imshow(np.abs(s2[j, :, :]), aspect=aspect)
                fig.colorbar(p, ax=ax[j+4, 0])

            ax[0, 1].set_title('Ψ↓1+Ψ↓†2')
            p = ax[0, 1].imshow(
                np.abs(s1[1, :, :] + s2[2, :, :]), aspect=aspect)
            fig.colorbar(p, ax=ax[0, 1])
            ax[1, 1].set_title('Ψ↓1-Ψ↓†2')
            p = ax[1, 1].imshow(
                np.abs(s1[1, :, :] - s2[2, :, :]), aspect=aspect)
            fig.colorbar(p, ax=ax[1, 1])
            ax[2, 1].set_title('Ψ↓2+Ψ↓†1')
            p = ax[2, 1].imshow(
                np.abs(s2[1, :, :] + s1[2, :, :]), aspect=aspect)
            fig.colorbar(p, ax=ax[2, 1])
            ax[3, 1].set_title('Ψ↓2-Ψ↓†1')
            p = ax[3, 1].imshow(
                np.abs(s2[1, :, :] - s1[2, :, :]), aspect=aspect)
            fig.colorbar(p, ax=ax[3, 1])
            ax[4, 1].set_title('Ψ↑1+Ψ↑†2')
            p = ax[4, 1].imshow(
                np.abs(s1[1, :, :] - s2[3, :, :]), aspect=aspect)
            fig.colorbar(p, ax=ax[4, 1])
            ax[5, 1].set_title('Ψ↑1-Ψ↑†2')
            p = ax[5, 1].imshow(
                np.abs(s1[1, :, :] + s2[3, :, :]), aspect=aspect)
            fig.colorbar(p, ax=ax[5, 1])
            ax[6, 1].set_title('Ψ↑2+Ψ↑†1')
            p = ax[6, 1].imshow(
                np.abs(s2[1, :, :] - s1[3, :, :]), aspect=aspect)
            fig.colorbar(p, ax=ax[6, 1])
            ax[7, 1].set_title('Ψ↑2-Ψ↑†1')
            p = ax[7, 1].imshow(
                np.abs(s2[1, :, :] + s1[3, :, :]), aspect=aspect)
            fig.colorbar(p, ax=ax[7, 1])

            ax[0, 2].set_title('Ψ↓1+Ψ↑†2')
            p = ax[0, 2].imshow(
                np.abs(s1[1, :, :] - s2[3, :, :]), aspect=aspect)
            fig.colorbar(p, ax=ax[0, 2])
            ax[1, 2].set_title('Ψ↓1-Ψ↑†2')
            p = ax[1, 2].imshow(
                np.abs(s1[1, :, :] + s2[3, :, :]), aspect=aspect)
            fig.colorbar(p, ax=ax[1, 2])
            ax[2, 2].set_title('Ψ↓2+Ψ↑†1')
            p = ax[2, 2].imshow(
                np.abs(s2[1, :, :] - s1[3, :, :]), aspect=aspect)
            fig.colorbar(p, ax=ax[2, 2])
            ax[3, 2].set_title('Ψ↓2-Ψ↑†1')
            p = ax[3, 2].imshow(
                np.abs(s2[1, :, :] + s1[3, :, :]), aspect=aspect)
            fig.colorbar(p, ax=ax[3, 2])
            ax[4, 2].set_title('Ψ↑1+Ψ↓†2')
            p = ax[4, 2].imshow(
                np.abs(s1[1, :, :] + s2[2, :, :]), aspect=aspect)
            fig.colorbar(p, ax=ax[4, 2])
            ax[5, 2].set_title('Ψ↑1-Ψ↓†2')
            p = ax[5, 2].imshow(
                np.abs(s1[1, :, :] - s2[2, :, :]), aspect=aspect)
            fig.colorbar(p, ax=ax[5, 2])
            ax[6, 2].set_title('Ψ↑2+Ψ↓†1')
            p = ax[6, 2].imshow(
                np.abs(s2[1, :, :] + s1[2, :, :]), aspect=aspect)
            fig.colorbar(p, ax=ax[6, 2])
            ax[7, 2].set_title('Ψ↑2-Ψ↓†1')
            p = ax[7, 2].imshow(
                np.abs(s2[1, :, :] - s1[2, :, :]), aspect=aspect)
            fig.colorbar(p, ax=ax[7, 2])

            plt.tight_layout()
            plt.savefig(f'{path}/{i//2:06}.png')
            plt.close()
    except:
        traceback.print_exc()
