#!/bin/bash

set -ex

SESSION="mbs-$(date +%Y%m%d-%H%M%S)"

[ -n "$HOST" ]
[ -n "$REMOTE_DIR" ]

if [ -n "$1" ]; then
    cargo +nightly check --bin "$1"
    CMD="cargo +nightly update; cargo +nightly run --release --bin $1 --no-default-features"
else
    cargo +nightly check
    CMD="cargo +nightly update; cargo +nightly run --release --no-default-features"
fi

ssh "$HOST" mkdir -p "$REMOTE_DIR"
rsync -avh --exclude='/.git' --filter="dir-merge,- .gitignore" ./ "$HOST:$REMOTE_DIR"
ssh "$HOST" "bash -l -c \"curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -q -y\"" || true
# scp config.yaml "$HOST:$REMOTE_DIR"
ssh "$HOST" "bash -l -c 'rustup install nightly'" || true
ssh "$HOST" "tmux new-session -s \"$SESSION\" -c \"$REMOTE_DIR\" -d bash -l -c \"$CMD || read\""
#ssh "$HOST" "cd $REMOTE_DIR; bash -l -c '$CMD'"
ssh -t "$HOST" tmux attach-session -t "$SESSION"

./sync.sh "$HOST"
#while true; do
# rsync -avh "$HOST:/scratch/local/dhauck/data/" ~/Dokumente/phd_data/ || true
# pipenv run ./plot/effective1d.py
# ./plot/edge_mode.py
#./plot/field.py
#./plot/test_array.py
#sleep 1m
#done
