#!/bin/bash

set -ex

cargo run --release
rm out/plt/* || true
./plot/test_array.py
mogrify -trim out/plt/*.png
eog out/plt/spectrum.png
