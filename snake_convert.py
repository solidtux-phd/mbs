#!/usr/bin/env python3

from json.decoder import JSONDecodeError
import sys
import re
import json
from glob import glob

dir = sys.argv[1]

def convert(camel_input):
    words = re.findall(r'[A-Z]?[a-z]+|[A-Z]{2,}(?=[A-Z][a-z]|\d|\W|$)|\d+', camel_input)
    return '_'.join(map(str.lower, words))

def convert_dict(d, depth=0):
    if isinstance(d, list):
        for l in d:
            convert_dict(l, depth=depth+1)
    elif isinstance(d, dict):
        keys = list(d.keys())
        for k in keys:
            k_camel = convert(k)
            if k != k_camel:
                d[k_camel] = d.pop(k)
                for _ in range(depth):
                    print('\t', end='')
                print(f'{k} -> {k_camel}')
            convert_dict(d[k_camel], depth=depth+1)

for fn in glob(f'{dir}/**/*.json', recursive=True):
    print(fn)
    try:
        with open(fn, 'r') as f:
            data = json.load(f)
            convert_dict(data)
        with open(fn, 'w') as f:
            json.dump(data, f, indent=4, sort_keys=True)
    except JSONDecodeError:
        print('\tinvalid JSON')