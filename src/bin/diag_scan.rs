use chrono::Local;
use log::LevelFilter;
use masterdb::vergen_session;
use mbs::*;
use ndarray::prelude::*;
use std::{f64::consts::FRAC_PI_2, fs, sync::Arc, thread};

const T: f64 = 0.5;
const MU: f64 = 0.;
const DELTA: f64 = 1.;
const ALPHA: f64 = 0.3;

const WIDTH: f64 = 1.;
const SIZE: (usize, usize) = (61, 61);

const TAG: &str = "diag-scan";
const OUTPUT: &str = "/scratch/local/dhauck/data";

fn main() -> Result<()> {
    let mut conf = Configuration {
        output: OUTPUT.into(),
        nev: 16,
        tag: TAG.into(),
        vectors: true,
        ..Configuration::default()
    };

    // rayon::ThreadPoolBuilder::new()
    //     .num_threads(2)
    //     .build_global()
    //     .unwrap();
    fs::create_dir_all(&conf.output)?;
    let mut log_path = conf.output.clone();
    log_path.push(format!(
        "{}_diag-scan.log",
        Local::now().format("%Y-%m-%d_%H-%M-%S")
    ));
    initialize_logging(log_path, LevelFilter::Debug)?;
    let structure = Structure::Box { width: WIDTH };

    for amplitude in Array1::linspace(0., 2., 201) {
        let magnetization = DomainWall {
            rotation: FRAC_PI_2,
            structure: structure.clone(),
            amplitude,
            direction: Direction::Y,
        };
        let hamiltonian = Hamiltonian {
            size: SIZE,
            t: T,
            mu: MU,
            sc_gap: DELTA,
            sc_gap_modifier: None,
            spin_orbit: ALPHA,
            magnetic_field: array![0., -amplitude, 0.],
            magnetization: Arc::new(magnetization.clone()),
            ..Hamiltonian::default()
        };

        conf.hamiltonians.push(hamiltonian.clone());
    }

    let session = vergen_session!();
    thread::spawn(move || {
        initialize_panics();
        conf.run(session)
    })
    .join()
    .unwrap();

    Ok(())
}
