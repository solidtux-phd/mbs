use std::{
    collections::HashMap,
    f64::{consts::FRAC_PI_2, NAN},
    fs::{self, File},
    io::Write,
    sync::{mpsc, Arc, Mutex},
    thread,
    time::Duration,
};

use chrono::Local;
#[allow(unused)]
use log::{debug, info, LevelFilter};
use masterdb::{vergen_session, Session};
use mbs::*;
use ndarray::prelude::*;
use ndarray_linalg::Trace;
use num_complex::Complex64;
use rand::{prelude::*, thread_rng};
use rayon::prelude::*;

// TODO read from configuration
const NX: usize = 61;
const N2: (usize, usize) = (21, 21);
const A: f64 = 0.2;
const M: f64 = 1./(A*A);
const MU: f64 = 0.;
const DELTA: f64 = 1.;
const ALPHA: f64 = 0.3;
const WIDTH: f64 = 1.;
const SCALE: f64 = 0.1;
const LAMBDA: f64 = ALPHA / A;

const T: f64 = 1. / (2. * M * A * A);

const NEV: usize = 8;
const N: usize = 2;
const BMIN: f64 = 0.;
const BMAX: f64 = -1.6;
const BPOINTS: usize = 20;
const MMIN: f64 = 0.;
const MMAX: f64 = 1.6;
const MPOINTS: usize = 20;
const TAG: &str = "effective1d-a0.1";
const OUTPUT: &str = "/scratch/local/dhauck/data";
// const OUTPUT: &str = "/tmp/data";

struct CalcResult {
    spectrum_sorted: Vec<f64>,
    spectrum2_sorted: Vec<f64>,
    v10: Array2<Complex64>,
    v11: Array2<Complex64>,
    v12: Array2<Complex64>,
    v20: Array2<Complex64>,
    v21: Array2<Complex64>,
    v22: Array2<Complex64>,
    v23: Array2<Complex64>,
    v24: Array2<Complex64>,
    pol_sum: Vec<f64>,
    pol_max: Vec<f64>,
}

fn calc(
    session: Session,
    mut conf: Configuration,
    magnetic_field: f64,
    amplitude: f64,
    rotate: bool,
) -> Result<CalcResult> {
    let sigma_x: Array2<Complex64> = array![
        [0., 1., 0., 0.], //
        [1., 0., 0., 0.], //
        [0., 0., 0., 1.], //
        [0., 0., 1., 0.], //
    ]
    .map(|x| x.into());
    let _tau_y: Array2<Complex64> = array![
        [0., 0., -1., 0.], //
        [0., 0., 0., -1.], //
        [1., 0., 0., 0.],  //
        [0., 1., 0., 0.],  //
    ]
    .map(|x| Complex64::i() * x);
    let tau_z: Array2<Complex64> = array![
        [1., 0., 0., 0.], //
        [0., 1., 0., 0.],
        [0., 0., -1., 0.],
        [0., 0., 0., -1.],
    ]
    .map(|x| x.into());

    for ham in conf.hamiltonians.iter_mut() {
        let structure = if rotate {
            Structure::Wang {
                width: WIDTH,
                scale: SCALE
            }
        } else {
            Structure::Box { width: WIDTH }
        };
        ham.magnetic_field = array![0., magnetic_field, 0.];
        ham.magnetization = Arc::new(DomainWall {
            structure,
            rotation: FRAC_PI_2,
            amplitude,
            direction: Direction::Y,
        });
    }

    conf.nev = 4 * NX;
    let (spectrum, states, ham) = conf.run_single(0, session.clone())?;
    conf.nev = NEV;
    let (ref_spectrum, states2, _ham2) = conf.run_single(1, session)?;
    let states = states.unwrap();
    let states2 = states2.unwrap();
    let indices = {
        let mut tmp = spectrum.iter().enumerate().collect::<Vec<_>>();
        tmp.sort_unstable_by(|x, y| x.1.abs().partial_cmp(&y.1.abs()).unwrap());
        for i in (0..tmp.len()).step_by(2) {
            if tmp[i].1 < tmp[i + 1].1 {
                tmp.swap(i, i + 1);
            }
        }
        tmp.into_iter().map(|x| x.0).collect::<Vec<_>>()
    };
    let indices2 = {
        let mut tmp = ref_spectrum.iter().enumerate().collect::<Vec<_>>();
        tmp.sort_unstable_by(|x, y| x.1.abs().partial_cmp(&y.1.abs()).unwrap());
        for i in (0..tmp.len()).step_by(2) {
            if tmp[i].1 < tmp[i + 1].1 {
                tmp.swap(i, i + 1);
            }
        }
        tmp.into_iter().map(|x| x.0).collect::<Vec<_>>()
    };

    // Majorana polarisation
    let mut pol_sum = vec![0.0_f64; NEV];
    let mut pol_max = vec![0.0_f64; NEV];
    for (i_ev, &i) in indices2.iter().enumerate() {
        let psi = states2
            .index_axis(Axis(1), i)
            .to_owned()
            .into_shape((N2.1, N2.0, 4))
            .unwrap();
        for j in 0..N2.1 {
            for k in 0..N2.0 {
                let p = 2.
                    * (psi[[j, k, 0]] * psi[[j, k, 3]].conj()
                        + psi[[j, k, 1]] * psi[[j, k, 2]].conj())
                    .norm();
                pol_sum[i_ev] += p;
                pol_max[i_ev] = pol_max[i_ev].max(p);
            }
        }
    }

    // Perturbation theory
    // constant part
    let v0 = tau_z.clone() * MU;
    // linear part
    let v1 = sigma_x.dot(&tau_z) * ham.spin_orbit;
    // quadratic part
    let v2 = tau_z / (2. * M);
    // first order corrections
    let mut v10: Array2<Complex64> = Array2::zeros((N, N));
    let mut v11: Array2<Complex64> = Array2::zeros((N, N));
    let mut v12: Array2<Complex64> = Array2::zeros((N, N));
    // second order corrections
    let mut v20: Array2<Complex64> = Array2::zeros((N, N));
    let mut v21: Array2<Complex64> = Array2::zeros((N, N));
    let mut v22: Array2<Complex64> = Array2::zeros((N, N));
    let mut v23: Array2<Complex64> = Array2::zeros((N, N));
    let mut v24: Array2<Complex64> = Array2::zeros((N, N));
    for i in 0..N {
        let psii = states
            .index_axis(Axis(1), indices[i])
            .to_owned()
            .into_shape((NX, 4))?;

        v10[[i, i]] = Complex64::new(spectrum[indices[i]], 0.);
        for j in 0..N {
            let psij = states
                .index_axis(Axis(1), indices[j])
                .to_owned()
                .into_shape((NX, 4))?;
            for k in 0..NX {
                v10[[i, j]] += psii
                    .index_axis(Axis(0), k)
                    .map(|x| x.conj())
                    .dot(&v0.dot(&psij.index_axis(Axis(0), k)));
                v11[[i, j]] += psii
                    .index_axis(Axis(0), k)
                    .map(|x| x.conj())
                    .dot(&v1.dot(&psij.index_axis(Axis(0), k)));
                v12[[i, j]] += psii
                    .index_axis(Axis(0), k)
                    .map(|x| x.conj())
                    .dot(&v2.dot(&psij.index_axis(Axis(0), k)));
                for l in N..spectrum.len() {
                    let psil = states
                        .index_axis(Axis(1), indices[l])
                        .to_owned()
                        .into_shape((NX, 4))?;
                    // constant
                    v20[[i, j]] += 0.5
                        * psii
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v0.dot(&psil.index_axis(Axis(0), k)))
                        * psil
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v0.dot(&psij.index_axis(Axis(0), k)))
                        * (1. / (spectrum[indices[i]] - spectrum[indices[l]])
                            + 1. / (spectrum[indices[j]] - spectrum[indices[l]]));
                    v20[[i, j]] += 0.5
                        * psii
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v0.dot(&psil.index_axis(Axis(0), k)))
                        * psil
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v0.dot(&psij.index_axis(Axis(0), k)))
                        * (1. / (spectrum[indices[i]] - spectrum[indices[l]])
                            + 1. / (spectrum[indices[j]] - spectrum[indices[l]]));
                    // linear
                    v21[[i, j]] += 0.5
                        * psii
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v1.dot(&psil.index_axis(Axis(0), k)))
                        * psil
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v0.dot(&psij.index_axis(Axis(0), k)))
                        * (1. / (spectrum[indices[i]] - spectrum[indices[l]])
                            + 1. / (spectrum[indices[j]] - spectrum[indices[l]]));
                    v21[[i, j]] += 0.5
                        * psii
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v0.dot(&psil.index_axis(Axis(0), k)))
                        * psil
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v1.dot(&psij.index_axis(Axis(0), k)))
                        * (1. / (spectrum[indices[i]] - spectrum[indices[l]])
                            + 1. / (spectrum[indices[j]] - spectrum[indices[l]]));
                    // quadratic
                    v22[[i, j]] += 0.5
                        * psii
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v1.dot(&psil.index_axis(Axis(0), k)))
                        * psil
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v1.dot(&psij.index_axis(Axis(0), k)))
                        * (1. / (spectrum[indices[i]] - spectrum[indices[l]])
                            + 1. / (spectrum[indices[j]] - spectrum[indices[l]]));
                    v22[[i, j]] += 0.5
                        * psii
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v0.dot(&psil.index_axis(Axis(0), k)))
                        * psil
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v2.dot(&psij.index_axis(Axis(0), k)))
                        * (1. / (spectrum[indices[i]] - spectrum[indices[l]])
                            + 1. / (spectrum[indices[j]] - spectrum[indices[l]]));
                    v22[[i, j]] += 0.5
                        * psii
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v2.dot(&psil.index_axis(Axis(0), k)))
                        * psil
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v0.dot(&psij.index_axis(Axis(0), k)))
                        * (1. / (spectrum[indices[i]] - spectrum[indices[l]])
                            + 1. / (spectrum[indices[j]] - spectrum[indices[l]]));
                    // cubic
                    v23[[i, j]] += 0.5
                        * psii
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v1.dot(&psil.index_axis(Axis(0), k)))
                        * psil
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v2.dot(&psij.index_axis(Axis(0), k)))
                        * (1. / (spectrum[indices[i]] - spectrum[indices[l]])
                            + 1. / (spectrum[indices[j]] - spectrum[indices[l]]));
                    v23[[i, j]] += 0.5
                        * psii
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v2.dot(&psil.index_axis(Axis(0), k)))
                        * psil
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v1.dot(&psij.index_axis(Axis(0), k)))
                        * (1. / (spectrum[indices[i]] - spectrum[indices[l]])
                            + 1. / (spectrum[indices[j]] - spectrum[indices[l]]));
                    // quartic
                    v24[[i, j]] += 0.5
                        * psii
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v2.dot(&psil.index_axis(Axis(0), k)))
                        * psil
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v2.dot(&psij.index_axis(Axis(0), k)))
                        * (1. / (spectrum[indices[i]] - spectrum[indices[l]])
                            + 1. / (spectrum[indices[j]] - spectrum[indices[l]]));
                    v24[[i, j]] += 0.5
                        * psii
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v2.dot(&psil.index_axis(Axis(0), k)))
                        * psil
                            .index_axis(Axis(0), k)
                            .map(|x| x.conj())
                            .dot(&v2.dot(&psij.index_axis(Axis(0), k)))
                        * (1. / (spectrum[indices[i]] - spectrum[indices[l]])
                            + 1. / (spectrum[indices[j]] - spectrum[indices[l]]));
                }
            }
        }
    }
    v20 += &v10;
    v21 += &v11;
    v22 += &v12;
    let mut spectrum_sorted: Vec<f64> = spectrum.map(|x| x.abs()).into_iter().collect();
    spectrum_sorted.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());
    let mut spectrum2_sorted: Vec<f64> = ref_spectrum.map(|x| x.abs()).into_iter().collect();
    spectrum2_sorted.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());
    // effective quantities
    Ok(CalcResult {
        spectrum_sorted,
        spectrum2_sorted,
        v10,
        v11,
        v12,
        v20,
        v21,
        v22,
        v23,
        v24,
        pol_sum,
        pol_max,
    })
}

fn save(
    session: &Session,
    conf: &Configuration,
    res: Arc<Mutex<HashMap<String, Array2<f64>>>>,
) -> Result<()> {
    let res = res.lock().unwrap();
    info!("Saving ...");
    for (name, data) in res.iter() {
        let mut path = conf.output.clone();
        path.push(format!(
            "{}_{}",
            session.run.date.format("%Y-%m-%d_%H-%M-%S"),
            TAG
        ));
        let _ = fs::create_dir_all(&path);
        path.push(format!("{}.csv", name));
        let mut f = File::create(&path).unwrap();
        for y in 0..MPOINTS {
            for x in 0..2 * BPOINTS {
                if x > 0 {
                    write!(f, ",")?;
                }
                write!(f, "{}", data[[y, x]])?;
            }
            writeln!(f)?;
        }
        info!("Saved {} to {}", name, path.display());
    }
    Ok(())
}

fn main() -> Result<()> {
    let conf = Configuration {
        hamiltonians: vec![
            Hamiltonian {
                size: (NX, 1),
                t: T,
                mu: MU + 2. * T.abs(),
                sc_gap: DELTA,
                spin_orbit: LAMBDA,
                ..Default::default()
            },
            Hamiltonian {
                size: N2,
                t: T,
                mu: MU,
                sc_gap: DELTA,
                spin_orbit: LAMBDA,
                periodic: (false, false),
                // magnetization_modifier: Some(Arc::new(Rectangle::new(None, HEIGHT))),
                ..Hamiltonian::default()
            },
        ],
        output: OUTPUT.into(),
        nev: NEV,
        tag: TAG.into(),
        vectors: true,
    };
    fs::create_dir_all(&conf.output)?;
    let mut log_path = conf.output.clone();
    log_path.push(format!(
        "{}_{}.log",
        Local::now().format("%Y-%m-%d_%H-%M-%S"),
        TAG
    ));
    initialize_logging(log_path, LevelFilter::Debug)?;
    let session = vergen_session!();
    let magnetic_fields = Array1::linspace(BMIN, BMAX, BPOINTS);
    let amplitudes = Array1::linspace(MMIN, MMAX, MPOINTS);
    let _ = fs::create_dir_all(&conf.output);

    let results: Arc<Mutex<HashMap<String, Array2<f64>>>> = Arc::new(Mutex::new(HashMap::new()));

    save(&session, &conf, results.clone()).unwrap();

    let (tx, rx) = mpsc::channel();
    let save_handle = {
        let conf = conf.clone();
        let results = results.clone();
        let session = session.clone();
        thread::spawn(move || loop {
            save(&session, &conf, results.clone()).unwrap();
            if rx.try_recv().is_ok() {
                break;
            }
            thread::sleep(Duration::from_secs(10));
        })
    };

    let mut mindvec: Vec<usize> = (0..MPOINTS).collect();
    mindvec.shuffle(&mut thread_rng());
    mindvec.into_par_iter().for_each(move |mind| {
        let amplitude = amplitudes[mind % MPOINTS];
        let conf = conf.clone();
        let magnetic_fields = magnetic_fields.clone();
        let results = results.clone();
        let session = session.clone();
        let mut bindvec: Vec<usize> = (0..2 * BPOINTS).collect();
        bindvec.shuffle(&mut thread_rng());
        bindvec.into_par_iter().for_each(move |bind| {
            let rotate = bind < BPOINTS;
            let p0: Array2<Complex64> = array![
                [1., 0.], //
                [0., 1.]
            ]
            .map(|x| x.into());
            let px: Array2<Complex64> = array![
                [0., 1.], //
                [1., 0.]
            ]
            .map(|x| x.into());
            let py: Array2<Complex64> = array![
                [0., -1.], //
                [1., 0.]
            ]
            .map(|x| Complex64::i() * x);
            let pz: Array2<Complex64> = array![
                [1., 0.], //
                [0., -1.]
            ]
            .map(|x| x.into());

            let magnetic_field = magnetic_fields[bind % BPOINTS];
            let res = calc(
                session.clone(),
                conf.clone(),
                magnetic_field,
                amplitude,
                rotate,
            )
            .unwrap();

            let mut list = Vec::new();

            macro_rules! pauli {
                ($e: ident) => {
                    let $e = [
                        res.$e.dot(&p0).trace().unwrap().re / 2.,
                        res.$e.dot(&px).trace().unwrap().re / 2.,
                        res.$e.dot(&py).trace().unwrap().re / 2.,
                        res.$e.dot(&pz).trace().unwrap().re / 2.,
                    ];
                    for i in 0..4 {
                        list.push((format!(concat!(stringify!($e), "-{}"), i), $e[i]))
                    }
                    {
                        list.push((
                            stringify!($e).to_string(),
                            $e.iter().map(|x| x.abs().powi(2)).sum::<f64>().sqrt(),
                        ));
                    }
                };
            }

            pauli!(v10);
            pauli!(v11);
            pauli!(v12);
            pauli!(v20);
            pauli!(v21);
            pauli!(v22);
            pauli!(v23);
            pauli!(v24);

            for i in 0..res.pol_sum.len() {
                list.push((format!("polsum{:03}", i), res.pol_sum[i]));
                list.push((format!("polmax{:03}", i), res.pol_max[i]));
            }
            list.push(("polsum".to_string(), res.pol_sum.iter().sum()));
            list.push(("polmax".to_string(), res.pol_max.iter().sum()));

            let t1 = v12[3];
            let t2 = v22[3];
            let mu1 = -v10[3];
            let mu2 = -(v10[3] + v20[3]);
            let delta = (v21[1] + Complex64::i() * v21[2]).norm();
            list.extend(
                [
                    ("mu1", mu1),
                    ("mu2", mu2),
                    ("delta", delta),
                    ("t1", t1),
                    ("t2", t2),
                    ("tmu1", mu1 * t1),
                    ("tmu2", mu2 * t2),
                    ("dtmu", delta - 4. * t2 * mu2),
                    (
                        "e0",
                        0.5 * (res.spectrum_sorted[0].abs() + res.spectrum_sorted[1].abs()),
                    ),
                    (
                        "e1",
                        0.5 * (res.spectrum_sorted[2].abs() + res.spectrum_sorted[3].abs()),
                    ),
                    (
                        "gap",
                        0.5 * (res.spectrum_sorted[2].abs() + res.spectrum_sorted[3].abs())
                            - 0.5 * (res.spectrum_sorted[0].abs() + res.spectrum_sorted[1].abs()),
                    ),
                    (
                        "e02",
                        0.5 * (res.spectrum2_sorted[0] + res.spectrum2_sorted[1]),
                    ),
                    (
                        "e12",
                        0.5 * (res.spectrum2_sorted[2] + res.spectrum2_sorted[3]),
                    ),
                    (
                        "gap2",
                        0.5 * (res.spectrum2_sorted[2] + res.spectrum2_sorted[3])
                            - 0.5 * (res.spectrum2_sorted[0] + res.spectrum2_sorted[1]),
                    ),
                    ("sgn1", (mu1 * t1).signum()),
                    ("sgn2", (mu2 * t2).signum()),
                    ("sgn4", (mu2 * v24[3]).signum()),
                ]
                .map(|(a, b)| (a.to_string(), b)),
            );
            for (name, d) in list {
                let mut res = results.lock().unwrap();
                let m = res
                    .entry(name.to_string())
                    .or_insert_with(|| Array2::from_elem((2 * MPOINTS, 2 * BPOINTS), NAN));
                m[[mind, bind]] = d;
            }
        });
    });

    tx.send(())?;
    save_handle.join().unwrap();

    Ok(())
}
