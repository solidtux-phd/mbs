use chrono::Local;
use log::LevelFilter;
use masterdb::vergen_session;
use mbs::*;
use ndarray::prelude::*;
use std::{f64::consts::FRAC_PI_2, fs, sync::Arc, thread};

const T: f64 = 0.5;
const MU: f64 = 0.;
const DELTA: f64 = 1.;
const ALPHA: f64 = 0.3;

// const WIDTH: f64 = 3.;
// const BOUNDARY: usize = 30;
// const SIZE: (usize, usize) = (60, 180);

// const WIDTH: f64 = 1.;
// const SCALE: f64 = 1.;
// const BOUNDARY: usize = 10;
// const SIZE: (usize, usize) = (20, 60);
// const DISTANCE: f64 = 20.;

const WIDTH: f64 = 1.;
const SCALE: f64 = 1.;
const BOUNDARY: usize = 10;
const SIZE: (usize, usize) = (60, 60);
const DISTANCE: f64 = 8.;

// const WIDTH: f64 = 1.;
// const SCALE: f64 = 1.;
// const BOUNDARY: usize = 10;
// const SIZE: (usize, usize) = (20, 20);
// const DISTANCE: f64 = 8.;

const TAG: &str = "paper-domainwall";
const OUTPUT: &str = "/scratch/local/dhauck/data";
// const OUTPUT: &str = "/tmp/paper-domainwall";

fn main() -> Result<()> {
    // let bc = (DELTA.powi(2) + MU.powi(2)).sqrt();

    let mut conf = Configuration {
        output: OUTPUT.into(),
        nev: 4,
        tag: TAG.into(),
        vectors: true,
        ..Configuration::default()
    };
    let mut conf_energy = Configuration {
        output: OUTPUT.into(),
        nev: 64,
        tag: format!("{}_energy", TAG),
        vectors: false,
        ..Configuration::default()
    };

    // rayon::ThreadPoolBuilder::new()
    //     .num_threads(2)
    //     .build_global()
    //     .unwrap();
    fs::create_dir_all(&conf.output)?;
    let mut log_path = conf.output.clone();
    log_path.push(format!(
        "{}_paper-domainwall.log",
        Local::now().format("%Y-%m-%d_%H-%M-%S")
    ));
    initialize_logging(log_path, LevelFilter::Debug)?;
    let half_width = SIZE.0 as f64 / 2.;
    let rectangle = Rectangle::new(None, Some(half_width));

    for (structure, rotations) in [
        (Structure::Box { width: WIDTH }, vec![0.]),
        (
            Structure::Wang {
                width: WIDTH,
                scale: SCALE,
            },
            vec![0., FRAC_PI_2],
        ),
    ] {
        for (amplitude, magnetic_field) in [
            (0.4, -0.4),
            (1., -1.),
            (1.1, -1.1),
            (1.3, -1.3),
            (1., -0.7),
            (0.7, -1.),
        ] {
            for &rotation in &rotations {
                let magnetization = DomainWall {
                    rotation,
                    structure: structure.clone(),
                    amplitude,
                    direction: Direction::Y,
                };
                let mag_comb = Combine {
                    magnetizations: vec![
                        (Arc::new(magnetization.clone()), (-DISTANCE / 2., 0.)),
                        (Arc::new(magnetization.clone()), (DISTANCE / 2., 0.)),
                    ],
                    background: array![0., amplitude, 0.],
                };
                let mut hamiltonian = Hamiltonian {
                    size: SIZE,
                    t: T,
                    mu: MU,
                    sc_gap: DELTA,
                    sc_gap_modifier: None,
                    spin_orbit: ALPHA,
                    magnetic_field: array![0., magnetic_field, 0.],
                    magnetization: Arc::new(magnetization.clone()),
                    ..Hamiltonian::default()
                };

                conf.hamiltonians.push(hamiltonian.clone());
                conf_energy.hamiltonians.push(hamiltonian.clone());
                hamiltonian.magnetization = Arc::new(mag_comb.clone());
                conf.hamiltonians.push(hamiltonian.clone());
                conf_energy.hamiltonians.push(hamiltonian.clone());

                // ES
                hamiltonian.size.1 += BOUNDARY;
                hamiltonian.magnetization_modifier = Some(Arc::new(rectangle.clone()));
                hamiltonian.magnetization = Arc::new(magnetization.clone());
                conf.hamiltonians.push(hamiltonian.clone());
                conf_energy.hamiltonians.push(hamiltonian.clone());
                hamiltonian.magnetization = Arc::new(mag_comb.clone());
                conf.hamiltonians.push(hamiltonian.clone());
                conf_energy.hamiltonians.push(hamiltonian.clone());

                // EM
                hamiltonian.magnetization_modifier = None;
                hamiltonian.magnetization = Arc::new(magnetization.clone());
                hamiltonian.sc_gap_modifier = Some(Arc::new(rectangle.clone()));
                conf.hamiltonians.push(hamiltonian.clone());
                conf_energy.hamiltonians.push(hamiltonian.clone());
                hamiltonian.magnetization = Arc::new(mag_comb.clone());
                conf.hamiltonians.push(hamiltonian.clone());
                conf_energy.hamiltonians.push(hamiltonian.clone());
            }
        }
    }

    let session = vergen_session!();

    let h = {
        let session = session.clone();
        thread::spawn(move || {
            initialize_panics();
            conf.run(session)
        })
    };
    let h_energy = thread::spawn(move || conf_energy.run(session));
    h.join().unwrap();
    h_energy.join().unwrap();

    Ok(())
}
