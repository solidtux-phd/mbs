#![feature(alloc_error_hook)]

use chrono::Local;
use fern::{colors::ColoredLevelConfig, Dispatch};
use log::{error, info, LevelFilter};
use masterdb::Session;
use ndarray::{prelude::*, OwnedRepr};
use ndarray_linalg::{EigValshInto, EighInto, Norm, Range, UPLO};
use num_complex::Complex64;
use rayon::prelude::*;
use serde::{Deserialize, Serialize};
use signal_hook::{
    consts::TERM_SIGNALS,
    iterator::{exfiltrator::WithOrigin, SignalsInfo},
};

use std::{
    alloc,
    fs::File,
    io,
    ops::RangeInclusive,
    path::{Path, PathBuf},
    sync::Arc,
    thread,
};

pub mod magnetization;
pub mod modifier;
pub use magnetization::*;
pub use modifier::*;

#[macro_export]
macro_rules! time {
    ($x:block) => {{
        let start = ::std::time::Instant::now();
        let res = $x;
        ::log::info!("{:?}", start.elapsed());
        res
    }};
    ($x:expr) => {
        time!({ $x })
    };
}

#[derive(Deserialize, Serialize, Clone, Default)]
pub struct Configuration {
    pub hamiltonians: Vec<Hamiltonian>,
    pub output: PathBuf,
    pub nev: usize,
    pub tag: String,
    pub vectors: bool,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct Entry {
    pub hamiltonian: Hamiltonian,
    pub output: PathBuf,
    pub nev: usize,
    pub tag: String,
    pub vectors: bool,
    pub index: usize,
}

impl Configuration {
    pub fn load() -> Result<Self> {
        Ok(serde_yaml::from_reader(File::open("config.yaml")?)?)
    }

    pub fn run_single(
        &self,
        i: usize,
        session: Session,
    ) -> Result<(Array1<f64>, Option<Array2<Complex64>>, Hamiltonian)> {
        let conf = Entry {
            hamiltonian: self.hamiltonians[i].clone(),
            output: self.output.clone(),
            nev: self.nev,
            tag: self.tag.clone(),
            vectors: self.vectors,
            index: i,
        };
        let mut ham = conf.hamiltonian.clone();
        let mut output_folder = conf.output.clone();
        output_folder.push(format!(
            "{}_{}",
            session.run.date.format("%Y-%m-%d_%H-%M-%S"),
            self.tag
        ));
        output_folder.push(format!("{:05}", i));

        let res = if conf.vectors {
            let (spectrum, states) = ham.states(conf.nev)?;
            let mut spectrum_file = output_folder.clone();
            spectrum_file.push("spectrum");
            session.save(spectrum_file, &conf, &[], &spectrum.clone().into_dyn())?;

            let mut state_file = output_folder.clone();
            state_file.push("state");
            session.save(state_file, &conf, &[], &states.clone().into_dyn())?;

            (spectrum, Some(states), ham.clone())
        } else {
            let spectrum = ham.spectrum(conf.nev)?;

            let mut spectrum_file = output_folder.clone();
            spectrum_file.push("spectrum");
            session.save(spectrum_file, &conf, &[], &spectrum.clone().into_dyn())?;
            (spectrum, None, ham.clone())
        };
        let mut magnetization_file = output_folder.clone();
        magnetization_file.push("magnetization");
        session.save(
            magnetization_file,
            &conf,
            &[],
            &ham.magnetization().into_dyn(),
        )?;
        Ok(res)
    }

    pub fn run(self, session: Session) {
        (0..self.hamiltonians.len()).into_par_iter().for_each(|i| {
            info!("Step {:>5}/{}", i, self.hamiltonians.len());
            if let Err(e) = self.run_single(i, session.clone()) {
                error!("{}", e);
            }
        });
    }
}

pub type Result<T> = ::std::result::Result<T, Box<dyn ::std::error::Error>>;

#[derive(Serialize, Deserialize, Clone)]
pub struct Hamiltonian {
    pub size: (usize, usize),
    pub t: f64,
    pub mu: f64,
    pub sc_gap: f64,
    pub sc_gap_modifier: Option<Arc<dyn Modifier>>,
    pub spin_orbit: f64,
    pub magnetic_field: Array1<f64>,
    pub magnetization: Arc<dyn Magnetization>,
    pub magnetization_modifier: Option<Arc<dyn Modifier>>,
    #[serde(skip)]
    pub magnetization_cache: Option<Array3<f64>>,
    pub distance: Option<f64>,
    pub periodic: (bool, bool),
}

impl Default for Hamiltonian {
    fn default() -> Self {
        Hamiltonian {
            size: (0, 0),
            t: 0.,
            mu: 0.,
            sc_gap: 0.,
            sc_gap_modifier: None,
            spin_orbit: 0.,
            magnetic_field: Array1::zeros(3),
            magnetization: Arc::new(()),
            magnetization_modifier: None,
            magnetization_cache: None,
            distance: None,
            periodic: (false, false),
        }
    }
}

impl Hamiltonian {
    fn index(&self, x: isize, y: isize, component: usize) -> usize {
        let x = ((x + self.size.0 as isize) % self.size.0 as isize) as usize;
        let y = ((y + self.size.1 as isize) % self.size.1 as isize) as usize;
        component + 4 * (x + self.size.0 * y)
    }

    pub fn slice(&self, x: isize, y: isize) -> RangeInclusive<usize> {
        self.index(x, y, 0)..=self.index(x, y, 3)
    }

    fn apply(&mut self, v1: ArrayView1<Complex64>, mut v2: ArrayViewMut1<Complex64>) {
        macro_rules! is {
            ($x:expr, $y:expr) => {
                s!(self.slice($x as isize, $y as isize))
            };
        }

        let sigma_x: Array2<Complex64> = array![
            [0., 1., 0., 0.], //
            [1., 0., 0., 0.], //
            [0., 0., 0., 1.], //
            [0., 0., 1., 0.], //
        ]
        .map(|x| x.into());
        let sigma_y: Array2<Complex64> = array![
            [0., -1., 0., 0.], //
            [1., 0., 0., 0.],  //
            [0., 0., 0., -1.], //
            [0., 0., 1., 0.],  //
        ]
        .map(|x| Complex64::i() * x);
        let sigma_z: Array2<Complex64> = array![
            [1., 0., 0., 0.], //
            [0., -1., 0., 0.],
            [0., 0., 1., 0.],
            [0., 0., 0., -1.],
        ]
        .map(|x| x.into());
        let tau_x: Array2<Complex64> = array![
            [0., 0., 1., 0.], //
            [0., 0., 0., 1.], //
            [1., 0., 0., 0.], //
            [0., 1., 0., 0.], //
        ]
        .map(|x| x.into());
        let _tau_y: Array2<Complex64> = array![
            [0., 0., -1., 0.], //
            [0., 0., 0., -1.], //
            [1., 0., 0., 0.],  //
            [0., 1., 0., 0.],  //
        ]
        .map(|x| Complex64::i() * x);
        let tau_z: Array2<Complex64> = array![
            [1., 0., 0., 0.], //
            [0., 1., 0., 0.],
            [0., 0., -1., 0.],
            [0., 0., 0., -1.],
        ]
        .map(|x| x.into());
        v2.fill(Complex64::new(0., 0.));
        let calc_mag = self.magnetization_cache.is_none();
        if calc_mag {
            self.magnetization_cache = Some(Array3::zeros((self.size.0, self.size.1, 3)));
        }
        for x in 0..self.size.0 {
            let xf = x as f64 - (self.size.0 - 1) as f64 / 2.;
            for y in 0..self.size.1 {
                let yf = y as f64 - (self.size.1 - 1) as f64 / 2.;

                let mut slice = v2.slice_mut(is!(x, y));
                let slice1 = v1.slice(is!(x, y));
                let xp = (x + 1) % self.size.0;
                let xm = (x + self.size.0 - 1) % self.size.0;
                let yp = (y + 1) % self.size.1;
                let ym = (y + self.size.1 - 1) % self.size.1;
                if x > 0 || self.periodic.0 {
                    slice -= &(Complex64::new(self.t, 0.) * tau_z.dot(&v1.slice(is!(xm, y))));
                    slice -= &(Complex64::new(0., self.spin_orbit / 2.)
                        * tau_z.dot(&sigma_y.dot(&v1.slice(is!(xm, y)))));
                }
                if x < self.size.0 - 1 || self.periodic.0 {
                    slice -= &(Complex64::new(self.t, 0.) * tau_z.dot(&v1.slice(is!(xp, y))));
                    slice += &(Complex64::new(0., self.spin_orbit / 2.)
                        * tau_z.dot(&sigma_y.dot(&v1.slice(is!(xp, y)))));
                }
                if y > 0 || self.periodic.1 {
                    slice -= &(Complex64::new(self.t, 0.) * tau_z.dot(&v1.slice(is!(x, ym))));
                    slice += &(Complex64::new(0., self.spin_orbit / 2.)
                        * tau_z.dot(&sigma_x.dot(&v1.slice(is!(x, ym)))));
                }
                if y < self.size.1 - 1 || self.periodic.1 {
                    slice -= &(Complex64::new(self.t, 0.) * tau_z.dot(&v1.slice(is!(x, yp))));
                    slice -= &(Complex64::new(0., self.spin_orbit / 2.)
                        * tau_z.dot(&sigma_x.dot(&v1.slice(is!(x, yp)))));
                }
                slice += &(Complex64::new(4. * self.t.abs() - self.mu, 0.) * tau_z.dot(&slice1));

                let mag = if calc_mag {
                    let mut m = match self.distance {
                        None => self.magnetization.evaluate(xf, yf),
                        Some(d) => (0..self.size.0)
                            .map(|x0| {
                                (0..self.size.1)
                                    .map(|y0| -> ArrayBase<OwnedRepr<f64>, Dim<[usize; 1]>> {
                                        let m = self.magnetization.evaluate(
                                            x0 as f64 - (self.size.0 - 1) as f64 / 2.,
                                            y0 as f64 - (self.size.1 - 1) as f64 / 2.,
                                        );
                                        let r =
                                            array![x as f64 - x0 as f64, y as f64 - y0 as f64, d];
                                        let rn = r.norm();
                                        (3. * r.clone() * m.dot(&r) / rn.powi(5) + m / rn.powi(3))
                                            * d.powi(3)
                                            / ((self.size.0 * self.size.1) as f64)
                                    })
                                    .fold(array![0., 0., 0.], |mut acc, x| {
                                        azip!((a in &mut acc, b in &x) *a += b);
                                        acc
                                    })
                            })
                            .fold(array![0., 0., 0.], |mut acc, x| {
                                azip!((a in &mut acc, b in &x) *a += b);
                                acc
                            }),
                    };
                    if let Some(x) = &self.magnetization_modifier {
                        m = x.evaluate(xf, yf, m);
                    }
                    self.magnetization_cache
                        .as_mut()
                        .unwrap()
                        .slice_mut(s![x, y, ..])
                        .assign(&m);
                    m
                } else {
                    self.magnetization_cache
                        .as_ref()
                        .unwrap()
                        .slice(s![x, y, ..])
                        .to_owned()
                };

                let gap = self
                    .sc_gap_modifier
                    .as_ref()
                    .map(|m| m.evaluate(xf, yf, array![self.sc_gap])[0])
                    .unwrap_or(self.sc_gap);

                slice += &(Complex64::new(self.magnetic_field[0], 0.) * sigma_x.dot(&slice1));
                slice += &(Complex64::new(self.magnetic_field[1], 0.) * sigma_y.dot(&slice1));
                slice += &(Complex64::new(self.magnetic_field[2], 0.) * sigma_z.dot(&slice1));
                slice += &(Complex64::new(gap, 0.) * tau_x.dot(&slice1));
                slice += &(Complex64::new(mag[[0]], 0.) * sigma_x.dot(&slice1));
                slice += &(Complex64::new(mag[[1]], 0.) * sigma_y.dot(&slice1));
                slice += &(Complex64::new(mag[[2]], 0.) * sigma_z.dot(&slice1));
            }
        }
    }

    pub fn matrix(&mut self) -> Result<Array2<Complex64>> {
        let n = self.size.0 * self.size.1 * 4;
        let mut res = Array2::zeros((n, n));
        for i in 0..n {
            let mut v = Array1::zeros((n,));
            v[[i]] = Complex64::new(1., 0.);
            self.apply(v.view(), res.slice_mut(s![.., i]));
        }
        Ok(res)
    }

    pub fn states(&mut self, nev: usize) -> Result<(Array1<f64>, Array2<Complex64>)> {
        info!("Generating matrix...");
        let mat = time!(self.matrix()?);
        info!("Diagonalizing matrix...");
        let n = mat.shape()[0];
        let lower = n / 2 - nev / 2 + 1;
        let upper = lower + nev - 1;
        let range = Range::Index(lower as i32, upper as i32);
        let (values, vectors) =
            time!(mat.eigh_range_into(UPLO::Upper, range, std::f64::EPSILON)?);
        info!("Postprocessing ...");
        let mut min_values: Vec<(usize, f64)> = values.to_vec().into_iter().enumerate().collect();
        min_values.sort_by(|a, b| a.1.abs().partial_cmp(&b.1.abs()).unwrap());
        let min_indices = min_values.into_iter().map(|x| x.0).take(nev);

        let n = 4 * self.size.0 * self.size.1;
        let mut res_values = Array1::zeros(nev);
        let mut res_vectors = Array2::zeros((n, nev));

        for (i, j) in min_indices.enumerate() {
            res_values[[i]] = values[j];
            res_vectors
                .slice_mut(s![.., i])
                .assign(&vectors.slice(s![.., j]));
        }

        Ok((res_values, res_vectors))
    }

    pub fn spectrum(&mut self, nev: usize) -> Result<Array1<f64>> {
        info!("Generating matrix...");
        let mat = time!(self.matrix()?);
        info!("Diagonalizing matrix...");
        let n = mat.shape()[0];
        let lower = n / 2 - nev / 2 + 1;
        let upper = lower + nev - 1;
        let range = Range::Index(lower as i32, upper as i32);
        let values = time!(mat.eigvalsh_range_into(UPLO::Upper, range, std::f64::EPSILON)?);
        //let values = time!(mat.eigvalsh_into(UPLO::Upper)?);
        Ok(values)
    }

    pub fn magnetization(&self) -> Array3<f64> {
        // TODO better error handling
        self.magnetization_cache.clone().unwrap()
    }
}

pub fn initialize_logging<P: AsRef<Path>>(log_path: P, level: LevelFilter) -> Result<()> {
    let colors = ColoredLevelConfig::default();
    Dispatch::new()
        .format(move |out, message, record| {
            out.finish(format_args!(
                "{}{}[{}][{:<5}] {}\x1B[0m",
                format_args!("\x1B[{}m", colors.get_color(&record.level()).to_fg_str()),
                Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                record.target(),
                record.level(),
                message
            ))
        })
        .level(level)
        .chain(io::stdout())
        .chain(fern::log_file(log_path)?)
        .apply()?;
    // initialize_panics();
    // handle_signals()?;
    Ok(())
}

pub fn initialize_panics() {
    log_panics::init();
    alloc::set_alloc_error_hook(|layout| {
        error!(
            "Allocation of {}GB failed.",
            layout.size() / 1024 / 1024 / 1024
        );
    });
}

pub fn handle_signals() -> Result<()> {
    let mut signals = SignalsInfo::<WithOrigin>::new(TERM_SIGNALS)?;
    // let flag = Arc::new(AtomicBool::new(false));
    // for sig in TERM_SIGNALS {
    //     flag::register(*sig, flag.clone())?;
    //     flag::register_conditional_shutdown(*sig, 1, flag.clone())?;
    // }
    thread::spawn(move || {
        for sig in signals.forever() {
            error!("Received signal {:?}", sig);
            signal_hook::low_level::exit(1);
        }
    });
    Ok(())
}
