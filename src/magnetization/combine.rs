use std::sync::Arc;

use crate::Magnetization;
use ndarray::Array1;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone)]
pub struct Combine {
    pub magnetizations: Vec<(Arc<dyn Magnetization>, (f64, f64))>,
    pub background: Array1<f64>,
}

#[typetag::serde(name = "Combine")]
impl Magnetization for Combine {
    fn evaluate(&self, x: f64, y: f64) -> Array1<f64> {
        let mut res: Array1<f64> = Array1::zeros(3);
        for (m, (dx, dy)) in &self.magnetizations {
            res = res + m.evaluate(x + dx, y + dy) - &self.background;
        }
        res + &self.background
    }
}
