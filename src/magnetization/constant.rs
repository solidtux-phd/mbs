use ndarray::prelude::*;
use serde::{Deserialize, Serialize};

use super::Magnetization;

#[derive(Serialize, Deserialize, Clone)]
pub struct Constant {
    pub width: Option<f64>,
    pub height: Option<f64>,
    pub field: [f64; 3],
}

#[typetag::serde(name = "Constant")]
impl Magnetization for Constant {
    fn evaluate(&self, x: f64, y: f64) -> Array1<f64> {
        let in_x = self.width.map(|x0| x.abs() <= x0).unwrap_or(true);
        let in_y = self.height.map(|y0| y.abs() <= y0).unwrap_or(true);
        if in_x && in_y {
            self.field.to_vec().into()
        } else {
            array![0., 0., 0.]
        }
    }
}
