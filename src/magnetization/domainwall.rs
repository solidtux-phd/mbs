use std::f64::consts::PI;

use ndarray::prelude::*;
use serde::{Deserialize, Serialize};

use super::Magnetization;

#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "snake_case")]
pub struct DomainWall {
    pub structure: Structure,
    pub rotation: f64,
    pub amplitude: f64,
    pub direction: Direction,
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "snake_case")]
pub enum Structure {
    Wang { width: f64, scale: f64 },
    Box { width: f64 },
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "snake_case")]
pub enum Direction {
    X,
    Y,
    Z,
}

#[typetag::serde(name = "domain_wall")]
impl Magnetization for DomainWall {
    fn evaluate(&self, x: f64, _y: f64) -> Array1<f64> {
        let phi = self.rotation;
        let theta = match self.structure {
            Structure::Wang { width, scale } => {
                2. * ((width / scale).sinh() / (x / scale).sinh()).atan()
            }
            Structure::Box { width } => {
                if x.abs() > width {
                    0.
                } else {
                    PI
                }
            }
        };
        match self.direction {
            Direction::X => {
                self.amplitude
                    * array![
                        theta.cos(),
                        theta.sin() * phi.cos(),
                        theta.sin() * phi.sin(),
                    ]
            }
            Direction::Y => {
                self.amplitude
                    * array![
                        theta.sin() * phi.sin(),
                        theta.cos(),
                        theta.sin() * phi.cos(),
                    ]
            }
            Direction::Z => {
                self.amplitude
                    * array![
                        theta.sin() * phi.cos(),
                        theta.sin() * phi.sin(),
                        theta.cos(),
                    ]
            }
        }
    }
}
