use ndarray::{array, Array1};

pub use combine::*;
pub use constant::*;
pub use domainwall::*;
pub use skyrmion::*;

mod combine;
mod constant;
mod domainwall;
mod skyrmion;

// TODO use rename_all when https://github.com/dtolnay/typetag/issues/4 is resolved
#[typetag::serde]
pub trait Magnetization: Sync + Send {
    fn evaluate(&self, x: f64, y: f64) -> Array1<f64>;
}

#[typetag::serde(name = "none")]
impl Magnetization for () {
    fn evaluate(&self, _x: f64, _y: f64) -> Array1<f64> {
        array![0., 0., 0.]
    }
}
