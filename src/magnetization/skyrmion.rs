use ndarray::prelude::*;
use serde::{Deserialize, Serialize};

use super::Magnetization;

#[derive(Serialize, Deserialize, Clone)]
pub struct Skyrmion {
    pub radius: (f64, f64),
    pub amplitude: f64,
    pub winding_number: isize,
    pub height: Option<f64>,
}

#[typetag::serde(name = "skyrmion")]
impl Magnetization for Skyrmion {
    fn evaluate(&self, x: f64, y: f64) -> Array1<f64> {
        if let Some(h) = self.height {
            if y.abs() > h {
                return array![0., 0., 0.];
            }
        }
        let xs = x / self.radius.0;
        let ys = y / self.radius.1;
        let rs = (xs.powi(2) + ys.powi(2)).sqrt();
        let phi = if rs == 0. { 0. } else { (self.winding_number as f64) * ys.atan2(xs) };
        let theta = 2. * (1. / rs.powi(2)).atan();

        self.amplitude
            * array![
                theta.sin() * phi.cos(),
                theta.sin() * phi.sin(),
                theta.cos()
            ]
    }
}
