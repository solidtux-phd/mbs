use chrono::Local;
use log::{info, LevelFilter};
use masterdb::vergen_session;
use mbs::*;
use std::{fs, time::Instant};

fn main() -> Result<()> {
    let session = vergen_session!();
    let conf = Configuration::load()?;

    let mut log_path = conf.output.clone();
    log_path.push(&conf.tag);
    fs::create_dir_all(&log_path)?;
    log_path.push(format!(
        "{}-{}.log",
        Local::now().format("%Y%m%d-%H%M%S"),
        session.run.host
    ));
    initialize_logging(log_path, LevelFilter::Debug)?;

    let start = Instant::now();
    conf.run(session);

    info!("{:?}", start.elapsed());
    Ok(())
}
