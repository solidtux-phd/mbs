use ndarray::Array1;

mod rectangle;

pub use rectangle::Rectangle;

#[typetag::serde]
pub trait Modifier: Sync + Send {
    fn evaluate(&self, x: f64, y: f64, value: Array1<f64>) -> Array1<f64>;
}
