use crate::Modifier;
use ndarray::Array1;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone)]
pub struct Rectangle {
    width: Option<f64>,
    height: Option<f64>,
}

impl Rectangle {
    pub fn new(width: Option<f64>, height: Option<f64>) -> Self {
        Self { width, height }
    }
}

#[typetag::serde]
impl Modifier for Rectangle {
    fn evaluate(&self, x: f64, y: f64, value: Array1<f64>) -> Array1<f64> {
        if self.width.map(|x0| x.abs() <= x0).unwrap_or(true)
            && self.height.map(|y0| y.abs() <= y0).unwrap_or(true)
        {
            value
        } else {
            Array1::zeros(value.raw_dim())
        }
    }
}
