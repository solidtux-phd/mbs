#!/bin/bash

set -ex

if [ -n "$1" ]; then
    for host in "$@"; do
        rsync -avh "$host:/scratch/local/dhauck/data/" ~/Dokumente/phd_data/ || true
    done
else
    for host in $(ncrun list -n); do
        rsync -avh "$host:/scratch/local/dhauck/data/" ~/Dokumente/phd_data/ || true
    done
fi
pushd ~/Dokumente/phd_data
git add --all || true
git commit -m 'Data update.' && git push || true
popd
pipenv run ./snake_convert.py ~/Dokumente/phd_data

# pipenv run ./plot/diag_scan.py
pipenv run ./plot/effective1d.py
# ./plot/edge_mode.py
# ./plot/test_array.py
# pipenv run ./plot/charge.py
# ./plot/skyrmion_scan.py
# ./plot/field.py
# ./plot/paper_domainwall.sh
# ./plot/spectrum.py
